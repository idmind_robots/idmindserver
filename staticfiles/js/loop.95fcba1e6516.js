var expanded = false;
var max_pages = 1;

function display_ros_msg(msg){
    document.getElementById("ros_msgs").innerHTML = "<p>"+msg+"<p>"
};

function select_tab(evt, tab_name){
    var tabs = document.getElementsByClassName("tabcontent");
    var tab_buttons = document.getElementsByClassName("tab_button");

    for (i=0; i < tabs.length; i++){
        tabs[i].style.display = "none";
    }
    document.getElementById(tab_name).style.display = "flex";

    for (i=0; i < tab_buttons.length; i++){
        tab_buttons[i].id = "";
    }
    evt.currentTarget.id="open";

};

var HttpClient = function() {
    this.get = function(aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function() {
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open( "GET", aUrl, true );
        anHttpRequest.send( null );
    }
}


function showCheckboxes(){
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "flex";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

function set_calendar_values(){
    // Generate default values for start and stop
    var now = new Date();
    var year = now.getFullYear();
    var month = now.getMonth()+1;
    var day = now.getDate();
    var start_str = year + "-" + (month < 10 ? "0" + month.toString() : month) + "-" +
                    (day < 10 ? "0" + day.toString() : day) + "T" + "00:00";
    var end_str = year + "-" + (month < 10 ? "0" + month.toString() : month) + "-" +
                    (day < 10 ? "0" + day.toString() : day) + "T" + "23:59";
    if(window.location.href.includes("?")){
        var params = window.location.href.split("?");
        var param_list = params[1].split("&");
        for(p in param_list){
            if(param_list[p].includes('start')){
                var aux = param_list[p].split('=')[1];
                if(aux.length > 1){
                    start_str = aux;
                }
            }
            if(param_list[p].includes('stop')){
                var aux = param_list[p].split('=')[1];
                if(aux.length > 1){
                    end_str = aux;
                }
            }
        }
    }
    start_str = start_str.replace("%3A", ":");
    end_str = end_str.replace("%3A", ":");
    // Assign values to input elements
    document.getElementById('start_input').value = start_str;
    document.getElementById('stop_input').value = end_str;
}

function change_page(page){
    console.log("Changing page")
    curr_location = window.location.href;
    if(curr_location.includes('page')){
        // Deconstruct the URL
        var parts = curr_location.split("?");
        var fields = parts[1].split("&");
        for(f in fields){
            if(fields[f].includes('page')){
                var page_parts = fields[f].split("=")
                var page_idx = f;
            }
        }
        // Reconstruct the URL
        var new_url = parts[0]+"?";
        for(f in fields){
            if(f != 0) new_url = new_url + "&";
            if(f == page_idx){
                new_url = new_url + page_parts[0]+"="+page;
            }
            else{
                new_url = new_url + fields[f];
            }
        }
        console.log(new_url);
        window.location.href = new_url;
    }
    else{
        if(curr_location.includes("?")){
            console.log(curr_location + "&page="+page);
            window.location.href = curr_location + "&page="+page;
        }
        else{
            console.log(curr_location + "?page="+page);
            window.location.href = curr_location + "?page="+page;
        }
    }

}