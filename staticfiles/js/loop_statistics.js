function list_years(){
    var start = 2017;
    var end = new Date().getFullYear();
    var options = "";
    for(var year = start ; year < end; year++){
        options += "<option>"+ year +"</option>";
    }
    options += "<option selected='selected'>"+ year +"</option>";
    document.getElementById("mission_year").innerHTML = options;
}

function list_weeks(){
    // Chosen year
    var curr_year = new Date().getFullYear();
    var year = document.getElementById("mission_year").value;

    // Last day to display
    var stop = new Date();
    if(year!=curr_year){
        // Month starts at 0
        stop = new Date(year, 11, 31);
    }

    var options = "";
    var date_opts = {day: "2-digit", year: "numeric", month: "short"};
    var start_day = getDateOfISOWeek(1, year)
    var stop_day = getDateOfISOWeek(1, year);
    stop_day.setDate(stop_day.getDate()+6);
    var week = 1;
    while(start_day < stop){
        if(stop_day < stop)
            options += "<option value='"+week+"'>"+start_day.toLocaleDateString("pt-PT", date_opts)+" - "+stop_day.toLocaleDateString("pt-PT", date_opts)+"</option>";
        else
        options += "<option selected='selected' value='"+week+"'>"+start_day.toLocaleDateString("pt-PT", date_opts)+" - "+stop_day.toLocaleDateString("pt-PT", date_opts)+"</option>";
        start_day.setDate(start_day.getDate()+7);
        stop_day.setDate(stop_day.getDate()+7);
        week = week + 1;

    }
    document.getElementById("mission_week").innerHTML = options;
}

function getDateOfISOWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4)
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    else
        ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    return ISOweekStart;
}


var HttpClient = function() {
    this.get = function(aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function() {
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open( "GET", aUrl, true );
        anHttpRequest.send( null );
    }
};

function display_plot(){
    // Set an HTTP CLient
    var client = new HttpClient();
    // Check year and week to get data from
    var week = document.getElementById("mission_week").value;
    var year = document.getElementById("mission_year").value;
    var start_day = getDateOfISOWeek(week, year);

    // Request Data
    start_date = start_day.getDate()
    start_month = start_day.getMonth()
    start_year = start_day.getFullYear()

    start_string = ""
    if(start_date < 10)
        start_string += "0";
    start_string += start_date;
    if(start_month < 9)
        start_string += "0";
    start_string += (start_month+1);
    start_string += start_year;

    client.get('/loop/week_stats?start_day='+start_string, function(response) {
        var week_missions = 0;
        var week_reboots = 0;
        var ctx = document.getElementById("missions_chart").getContext('2d');
        var options = {
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0.0,
                        max: 50.0,
                        beginAtZero:true,
                    }
                }],
                xAxes:[{
                    type: 'time',
                    ticks: {
                        source: 'labels',
                    },
                    time: {
                        unit: 'day',
                        displayFormats: {day: 'MMM DD'},
                    },
                    offset: true,

                }]
                }
            };
        var data = {
            labels: [],
            datasets: [{
                label: "Missions Completed",
                data: [],
                backgroundColor: 'rgba(0, 255, 0, 1)',
            },
            {
                label: "Missions Started",
                data: [],
                backgroundColor: 'rgba(0, 0,255, 1)',
            },
            {
                label: "Reboots",
                data: [],
                backgroundColor: 'rgba(255, 0, 0, 1)',
            },
            ]
        };

        json_resp = JSON.parse(response);
        console.log(response)
        /*data.labels.push(start_day);*/
        /*EDIT: Chart.js is not dealing well with timestamps*/
        console.log(data.labels);
        for(i=0;i<7;i++){
            log_day = new Date(start_day);
            log_day.setDate(start_day.getDate()+i)
            data.labels.push(new Date(log_day));
        }

        for(i=0;i<7;i++){
            data.datasets[0].data.push(json_resp["missions_completed"][i]);
            data.datasets[1].data.push(json_resp["missions_started"][i]);
            data.datasets[2].data.push(json_resp["reboots"][i]);
            week_missions += Number(json_resp["missions_completed"][i]);
            week_reboots += Number(json_resp["reboots"][i]);
        }

        var config = {
            type: 'bar',
            data: data,
            options: options
        };

        var myChart = new Chart(ctx, config);

        //update_statistics
        sday = start_day.getDate()
        eday = start_day.getDate()+6
        m = start_day.toLocaleString('en-EN', { month: 'long' });
        info_div = document.getElementById("week_stats");
        info_div.innerHTML = "<h3>Week Report - "+sday+" to "+eday+" of "+m+"</h3><p>"+week_missions+" mission(s) completed this week. <br /> Approximately "+(week_missions*.1).toFixed(2)+"Km travelled.<br /> It was rebooted "+week_reboots+" times";

    });

    client.get('/loop/month_stats?start_day='+start_string, function(response) {

        json_resp = JSON.parse(response);

        month_missions = Number(json_resp["missions_completed"]);
        month_reboots = Number(json_resp["reboots"]);

        //update_statistics
        m = start_day.toLocaleString('en-EN', { month: 'long' });
        info_div = document.getElementById("month_stats");
        info_div.innerHTML = "<h3>Month Report of "+m+"</h3><p>"+month_missions+" mission(s) completed this month. <br /> Approximately "+(month_missions*.1).toFixed(2)+"Km travelled.<br /> It was rebooted "+month_reboots+" times";
    });

    client.get('/loop/daily_stats', function(response) {

        json_resp = JSON.parse(response);

        day_missions = Number(json_resp["missions_completed"]);
        day_started = Number(json_resp["missions_started"]);
        day_reboots = Number(json_resp["reboots"]);

        //update_statistics
        d = start_day.toLocaleString('en-EN', {day: "2-digit", year: "numeric", month: "short"});
        info_div = document.getElementById("daily_stats");
        info_div.innerHTML = "<h3>Daily Report of "+d+"</h3><p>"+day_missions+" mission(s) completed of "+day_started+" started. <br /><br /> It was rebooted "+day_reboots+" times";
    });

};

function update_stats(){
    console.log("Getting Loop stats");
    var client = new HttpClient();
    // Check year and week to get data from
    var week = document.getElementById("mission_week").value;
    var year = document.getElementById("mission_year").value;
    var start_day = getDateOfISOWeek(week, year);

    // Request Data
    start_date = start_day.getDate()
    start_month = start_day.getMonth()
    start_year = start_day.getFullYear()

    start_string = ""
    if(start_date < 10)
        start_string += "0";
    start_string += start_date;
    if(start_month < 9)
        start_string += "0";
    start_string += (start_month+1);
    start_string += start_year;

    client.get('/loop/get_stats?start_day='+start_string, function(response) {
        console.log(response);
    }
}