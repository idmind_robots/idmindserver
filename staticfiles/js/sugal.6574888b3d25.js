/***********************/
/* Auxiliary functions */
/***********************/
function httpGetAsync(theUrl, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous
    xmlHttp.send(null);
}

function convert_date(fullDate, type){
    day = fullDate.getDate();
    if(fullDate.getDate() < 10)
        day = "0" + day;
    month = fullDate.getMonth()+1;
    if(fullDate.getMonth()+1 < 10)
        month = "0" + month;
    year = fullDate.getFullYear();

    if(type=="dmy")
        return day+"-"+month+"-"+year
    if(type=="ymd")
        return year+"-"+month+"-"+day
}
/*************/
/* GPS List  */
/*************/
function request_gps_list(order){
    // Get filters and order
    box_nr = document.getElementById("box_filter").value
    imei = document.getElementById("imei_filter").value
    server = document.getElementById("server_filter").value
    charging_true = document.getElementById("charging_filter_yes").checked
    charging_false = document.getElementById("charging_filter_no").checked
    // Get data (filtered and ordered)
    link = "/sugal/get_gps_list?order_by="+order

    if(box_nr!='')
        link += "&box_nr="+box_nr
    if(imei!='')
        link += "&imei="+imei
    if(server!='All')
        link += "&server="+server
    if(!(charging_true == charging_false)){
        link += "&charging="+(charging_true ? true : false)
    }
    httpGetAsync(link, update_gps_table)
}

function update_gps_table(response){
    resp = JSON.parse(response.responseText);

    // Get table and remove tbody
    var tb = document.getElementById("device_list_table");
    tb.removeChild(tb.getElementsByTagName("tbody")[0]);

    //Create new tbody
    var tbd = document.createElement("tbody");
    var gps_count = 0;
    var fields = ["box_nr", "imei", "server", "charging"];
    for (g in resp){
        gps_count +=1;
        var row = document.createElement("tr");
        for (f in fields){
            var td = document.createElement("td");
            td.innerHTML = resp[g][fields[f]];
            row.appendChild(td);
        }
        // Warning
        var td = document.createElement("td");
        light = (resp[g]['warning'] == 0) ? 'green' : ((resp[g]['warning'] == 1) ? 'yellow' : 'red');
        td.innerHTML = "<a href='detail/"+resp[g]['imei']+"'><img src='/static/img/"+light+"_light.jpg' height='15px' /></a>";
        row.appendChild(td);
        tbd.appendChild(row);
    }
    tb.appendChild(tbd);
    document.getElementById("gps_count").innerHTML ="<p>Showing "+gps_count+" devices</p>";
}

/***************/
/* GPS Detail  */
/***************/
function select_tab(evt, tab_name){
    var tabs = document.getElementsByClassName("tabcontent");
    var tab_buttons = document.getElementsByClassName("tab_button");

    for (i=0; i < tabs.length; i++){
        tabs[i].style.display = "none";
    }
    document.getElementById(tab_name).style.display = "flex";

    for (i=0; i < tab_buttons.length; i++){
        tab_buttons[i].id = "";
    }
    evt.currentTarget.id="open";

}

function update_plot_data(){
    date = document.getElementById("log_calendar").value
    request_plot_data(new Date(date));
}

function request_plot_data(plotDate){
    d = convert_date(plotDate, "dmy");
    imei = document.getElementById("detail_imei").value
    httpGetAsync("/sugal/detail/"+imei+"/get_bat_logs?day="+d, display_plot)
}

function display_plot(response){
    console.log(response.responseText)
    resp = JSON.parse(response.responseText);

    start = new Date(document.getElementById("log_calendar").value).setHours(0,0,1)
    end = new Date(document.getElementById("log_calendar").value).setHours(23,59,59)

    var ctx = document.getElementById("gpslog_chart").getContext('2d');
    var data = {
        labels: [],
        datasets: [{
            label: "Internal Battery",
            data: [],
            backgroundColor: 'rgba(0, 255, 0, 1)',
        },
        {
            label: "External Battery",
            data: [],
            backgroundColor: 'rgba(0, 0,255, 1)',
        }],
    };
    var options = {
        title: {
            display: true,
            text: document.getElementById("log_calendar").value,
        },
        scales: {
            yAxes: [{
                ticks: {
                    min: 0.0,
                    max: 6.0,
                    beginAtZero:true,
                },
                scaleLabel: {
                    display: true,
                    labelString: "Voltage",
                }
            }],
            xAxes:[{
                type: 'time',
                time: {
                    unit: 'hour',
                    displayFormats: {hour: 'hA'},
                    min: start,
                    max: end,
                },
                offset: true,
                scaleLabel: {
                    display: true,
                    labelString: "Time Of Day",
                }
            }]
        }
    };
    var config = {
        type: 'line',
        data: data,
        options: options
    };

    for(l in resp){
        data.labels.push(resp[l][0])
        data.datasets[0].data.push(resp[l][1])
        data.datasets[1].data.push(resp[l][2])
    }

    var myChart = new Chart(ctx, config);
}

/***********************************/
/*         LOGS TABLE              */
/***********************************/
function request_gps_logs(order){
    // Get filters and order
//    box_nr = document.getElementById("box_filter").value
    imei = document.getElementById("detail_imei").value
//    server = document.getElementById("server_filter").value
//    charging_true = document.getElementById("charging_filter_yes").checked
//    charging_false = document.getElementById("charging_filter_no").checked

    // Get data (filtered and ordered)
    link = "/sugal/detail/"+imei+"/get_gps_logs?order_by="+order
    httpGetAsync(link, update_logs_table)
}

function update_logs_table(response){
    resp = JSON.parse(response.responseText);
    console.log(resp)
    // Get table and remove tbody
    var tb = document.getElementById("log_list_table");
    tb.removeChild(tb.getElementsByTagName("tbody")[0]);

    //Create new tbody
    var tbd = document.createElement("tbody");
    for (g in resp){
        var row = document.createElement("tr");
        fields = ["timestamp_received", "timestamp_msg", "latitude", "longitude", "event", "speed", "valid", "battery", "ext_battery"];
        for (f in fields){
            var td = document.createElement("td");
            td.innerHTML = resp[g][fields[f]];
            row.appendChild(td);
        }
        tbd.appendChild(row);
//        THIS CODE WILL ADD THE RAW MESSAGE TO A NEW LINE IN THE TABLE
//        var row = document.createElement("tr");
//        var td = document.createElement("td");
//        td.colSpan = 9;
//        td.innerHTML = resp[g]['raw']+" | https://www.w3schools.com/css/css_tooltip.asp";
//        row.appendChild(td);
//        tbd.appendChild(row);
    }
    tb.appendChild(tbd);
}
