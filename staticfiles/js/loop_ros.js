function connect_to_ros(){
    display_ros_msg("Connecting to Loop...");
    var ros = new ROSLIB.Ros({
        url : 'ws://25.33.168.244:9090'
        //url : 'ws://localhost:9090'
    });

    ros.on('connection', function(){
        display_ros_msg("Connected to websocket server.");
    });

    ros.on('error', function(error) {
        display_ros_msg('Error connecting to websocket server: '+ error);
    });

    ros.on('close', function() {
        display_ros_msg('Connection to websocket server closed.');
    });

    var listener = new ROSLIB.Topic({
        ros : ros,
        name : '/chatter',
        messageType : 'std_msgs/String'
    });

    listener.subscribe(function(message) {
        console.log('Received message on ' + listener.name + ': ' + message.data);
        listener.unsubscribe();
    });

};
