function switch_display(menu){
    var menu_handle = document.getElementById(menu);
    var style = window.getComputedStyle(menu_handle);
    if(style.display=='flex')
        close_menu(menu);
    else if(style.display=='none')
        open_menu(menu);
}

function open_menu(menu){
    var menu_handle = document.getElementById(menu);
    menu_handle.style.display = 'flex';
}

function close_menu(menu){
    var menu_handle = document.getElementById(menu);
    menu_handle.style.display = 'none';
}