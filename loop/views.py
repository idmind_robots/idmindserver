from django.http import JsonResponse, HttpResponseServerError, HttpResponse, Http404, HttpResponseForbidden
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView
from command_center.models import RobotDetails, RobotLog
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from datetime import datetime, timedelta, date, time
from calendar import monthrange
from math import ceil
import json
import pytz

lx_tz = pytz.timezone("Portugal")


class LoopList(LoginRequiredMixin, ListView):
    """
    Class that returns the robots belonging to the Loop Project
    If there is only one robot, redirect to its detail view
    """
    login_url = '/accounts/login'
    model = RobotDetails
    queryset = RobotDetails.objects.filter(project__name="Loop")
    context_object_name = "loop_project_list"
    template_name = 'loop/loop.html'


class LoopDetail(LoginRequiredMixin, DetailView):
    login_url = '/accounts/login'
    model = RobotDetails
    context_object_name = "loop_detail"
    template_name = 'loop/loop.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["loop_project_list"] = RobotDetails.objects.filter(project__name="Loop")
        return context


class LoopLogList(LoginRequiredMixin, ListView):
    login_url = '/accounts/login'
    model = RobotLog
    context_object_name = "loop_log_list"
    template_name = 'loop/loop.html'
    paginate_by = 30

    def get_queryset(self):

        # Apply time filters
        try:
            start_time = lx_tz.localize(datetime.strptime(self.request.GET["start"], "%Y-%m-%dT%H:%M"))
        except KeyError:
            start_time = lx_tz.localize(datetime.combine(date.today(), time()))
        try:
            stop_time = lx_tz.localize(datetime.strptime(self.request.GET["stop"], "%Y-%m-%dT%H:%M"))
        except KeyError:
            stop_time = lx_tz.localize(datetime.combine(date.today(), time.max))
        logs = RobotLog.objects.filter(robot__name="Loop", timestamp__range=(start_time, stop_time)).order_by('-timestamp')

        # Apply node filters
        try:
            nodes = []
            if len(self.request.build_absolute_uri().split('?')) > 1:
                params = self.request.build_absolute_uri().split('?')[1]
                for p in params.split('&'):
                    if p.split('=')[0] == 'nodes':
                        nodes.append((p.split('=')[1]).replace("%2F", "/"))
                # If no filter is applied, the request could only have page number
                if len(nodes) > 0:
                    logs = logs.filter(node__in=nodes)
        except KeyError:
            print("No nodes were specified")


        return logs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["loop_log_flag"] = True
        context["loop_project_list"] = RobotDetails.objects.filter(project__name="Loop")
        context["loop_nodes"] = RobotLog.objects.filter(robot__name="Loop").order_by('node').values('node').distinct()
        return context


@login_required
def get_logs(request):
    # lx_tz = pytz.timezone("Portugal")
    log_dict = {}
    print("Getting Logs")
    try:
        if request.method == "GET":
            robot = RobotDetails.objects.get(name=request.GET["robot"])

            unodes = request.GET["node"].split("|")

            ustart = lx_tz.localize(datetime.strptime(request.GET["start"], "%d-%m-%Y %H:%M"))
            ustop = lx_tz.localize(datetime.strptime(request.GET["stop"], "%d-%m-%Y %H:%M"))
            page = int(request.GET["page"])
            total_per_page = 25

            log_dict = {}
            print(robot)
            print(unodes)
            print(ustart)
            print(ustop)
            logs = RobotLog.objects.\
                filter(robot=robot, node__in=unodes, timestamp__range=(ustart, ustop)).\
                order_by('-timestamp')
            print(logs)
            page_logs = logs[(page-1)*total_per_page:page*total_per_page]

            for idx, lg in enumerate(page_logs):
                log_dict.update({str(idx): {"node": lg.node,
                                            "day": lg.timestamp.astimezone(lx_tz).date(),
                                            "time": lg.timestamp.astimezone(lx_tz).time(),
                                            "data": lg.data}})
            log_dict.update({"max_pages": ceil(len(logs)/total_per_page)})

    except Exception as err:
        print("Exception occurred: "+str(err))
        return HttpResponseServerError()
    else:
        return JsonResponse(log_dict)


# @login_required
# def get_daily_statistics(request):
#     log_dict = {}
#     try:
#         print("Getting Daily Statistics")
#         if request.method == "GET":
#             start_time = datetime.combine(date.today(), time(hour=0, minute=0, second=0), tzinfo=lx_tz)
#             stop_time = datetime.combine(date.today(), time(hour=23, minute=59, second=59), tzinfo=lx_tz)
#             print("{} | {}".format(start_time, stop_time))
#             log_dict["missions_started"] = RobotLog.objects.filter(robot__name="Loop",
#                                                                    timestamp__range=(start_time, stop_time),
#                                                                    data__contains="Mission started").count()
#             log_dict["missions_completed"] = RobotLog.objects.filter(robot__name="Loop",
#                                                                      timestamp__range=(start_time, stop_time),
#                                                                      data__contains="Mission completed").count()
#             mission_logs = RobotLog.objects.filter(robot__name='Loop',
#                                                    timestamp__range=(start_time, stop_time),
#                                                    data__regex="Mission|restarting").order_by('timestamp').count()
#             print("Logs: {}".format(mission_logs))
#
#
#
#
#             log_dict["reboots"] = RobotLog.objects.filter(robot__name="Loop",
#                                                           timestamp__range=(start_time, stop_time),
#                                                           data__contains="***** Logging is restarting *****").count()
#         else:
#             print("Method not allowed")
#             return HttpResponse(status=403)
#     except Exception as err:
#         print("Exception: {}".format(err))
#         return HttpResponseServerError(err)
#     return JsonResponse(log_dict)


@login_required
def get_statistics(request):
    """
        Returns a dictionary with three fields: 'day, 'week', 'month'
        Each field contains 'start, 'stop','started', 'completed','reboots'
    """
    log_dict = {'msg': 'Communication'}
    try:
        start = datetime.now()
        if request.method == "GET":
            day = int(request.GET["start_day"][0:2])
            month = int(request.GET["start_day"][2:4])
            year = int(request.GET["start_day"][4:])

            # Get monthly stats
            start_time = datetime.combine(date(year, month, 1), time(hour=0, minute=0, second=0), tzinfo=lx_tz)
            stop_time = datetime.combine(date(year, month, monthrange(year, month)[1]), time(hour=23, minute=59, second=59), tzinfo=lx_tz)
            month_mission_logs = RobotLog.objects.filter(robot__name='Loop',
                                                   timestamp__range=(start_time, stop_time),
                                                   data__regex=r'^(Mission [a-zA-Z]+)|(.+ restarting .+)$').order_by('timestamp')

            log_day = datetime.today()
            start_week = datetime(year, month, day, tzinfo=lx_tz)
            # Search stops at 00:00 of Monday
            stop_week = start_week + timedelta(days=7)
            day_dict = {"start": log_day, "started": 0, "completed": 0, "reboots": 0}
            week_dict = {"start": start_week, "stop": stop_week, "started": 0, "completed": 0, "reboots": 0}
            month_dict = {"start": start_time, "stop": stop_time, "started": 0, "completed": 0, "reboots": 0}

            last = ""
            for log in month_mission_logs:
                is_today = (log.timestamp.date() == log_day.date())
                is_week = (start_week.date() <= log.timestamp.date() <= stop_week.date())
                if ("started" in log.data) and not ("started" in last):
                    month_dict["started"] += 1
                    if is_week:
                        week_dict["started"] += 1
                    if is_today:
                        day_dict["started"] += 1
                elif ("completed" in log.data) and ("started" in last):
                    month_dict["completed"] += 1
                    if is_week:
                        week_dict["completed"] += 1
                    if is_today:
                        day_dict["completed"] += 1
                elif "restarting" in log.data:
                    month_dict["reboots"] += 1
                    if is_week:
                        week_dict["reboots"] += 1
                    if is_today:
                        day_dict["reboots"] += 1
                last = log.data

            log_dict = {'day': day_dict, 'week': week_dict, 'month': month_dict}
        else:
            print("Method not allowed")
            return HttpResponse(status=403)
        print("General statistics compiled in {}".format((datetime.now()-start).total_seconds()))
    except Exception as err:
        print("Exception: {}".format(err))
        return HttpResponseServerError(err)
    return JsonResponse(log_dict)


@login_required
def get_week_statistics(request):
    """
        Return a dictionary with three fields: 'missions_started', 'missions_completed' and 'reboots'
        Each field has a list of number of events for each day
    """
    log_dict = {}
    try:
        print("Working on Week Statistics")
        start = datetime.now()
        if request.method == "GET":
            day = int(request.GET["start_day"][0:2])
            month = int(request.GET["start_day"][2:4])
            year = int(request.GET["start_day"][4:])
            naive_start = datetime(day=day, month=month, year=year, hour=00, minute=00, second=1)
            missions_started_list = [0] * 7
            missions_completed_list = [0] * 7
            reboots_list = [0] * 7
            start_day = lx_tz.localize(naive_start)
            end_day = lx_tz.localize(naive_start)+timedelta(days=7)
            print("Getting data between {} and {}".format(start_day, end_day))
            # Single access to the database
            day_mission_logs = RobotLog.objects.filter(robot__name='Loop',
                                                        timestamp__range=(start_day, end_day),
                                                        data__regex=r'^(Mission [a-zA-Z]+)|(.+ restarting .+)$').order_by('timestamp')

            last = ""
            for log in day_mission_logs:
                idx = (log.timestamp.date() - start_day.date()).days
                if ("started" in log.data) and not ("started" in last):
                    missions_started_list[idx] += 1
                elif ("completed" in log.data) and ("started" in last):
                    missions_completed_list[idx] += 1
                elif "restarting" in log.data:
                    reboots_list[idx] += 1
                last = log.data

            log_dict["missions_started"] = missions_started_list
            log_dict["missions_completed"] = missions_completed_list
            log_dict["reboots"] = reboots_list
        else:
            print("No information provided on GET")
        print("Week statistics compiled - {}secs".format((datetime.now()-start).total_seconds()))
    except Exception as err:
        print("Exception occurred: " + str(err))
        return HttpResponseServerError()
    else:
        return JsonResponse(log_dict)


# @login_required
# def get_month_statistics(request):
#     # lx_tz = pytz.timezone("Portugal")
#     log_dict = {}
#     try:
#         # print("Working on Month Statistics")
#         if request.method == "GET":
#             month = int(request.GET["start_day"][2:4])
#             year = int(request.GET["start_day"][4:])
#             start_day = lx_tz.localize(datetime.today().replace(day=1, month=month, year=year, hour=00, minute=00, second=1))
#             end_day = lx_tz.localize(datetime.today().replace(day=monthrange(year, month)[1], month=month, year=year, hour=00, minute=00, second=1))
#
#             missions_started = RobotLog.objects.filter(robot__name="Loop", timestamp__range=(start_day, end_day),
#                                                        data__contains="Mission started").count()
#             missions_completed = RobotLog.objects.filter(robot__name="Loop", timestamp__range=(start_day, end_day),
#                                                          data__contains="Mission completed").count()
#             reboots = RobotLog.objects.filter(robot__name="Loop", timestamp__range=(start_day, end_day),
#                                               data__contains="***** Logging is restarting *****").count()
#
#             log_dict["missions_started"] = missions_started
#             log_dict["missions_completed"] = missions_completed
#             log_dict["reboots"] = reboots
#         else:
#             print("No information provided on GET")
#     except Exception as err:
#         print("Exception occurred: " + str(err))
#         return HttpResponseServerError()
#     else:
#         return JsonResponse(log_dict)


@csrf_exempt
def add_log(request):
    print("Received a request to add new Loop log")
    # lx_tz = pytz.timezone("Portugal")
    try:
        if request.method == "POST":
            data = json.loads(request.body.decode("utf-8", "strict"))
            #  AUTHENTICATION  #
            # if "loop_username" in data:
            #     username = data["loop_username"]
            #     password = data["loop_password"]
            # else:
            #     print("Wrong credentials")
            #     return HttpResponseForbidden("Unable to find credentials")
            # print("Authenticating {} with {}".format(username, password))
            # user = authenticate(request, username=username, password=password)
            # if user is not None:
            #     login(request, user)
            #     ts = lx_tz.localize(datetime.strptime(data["timestamp"], "%Y-%m-%d %H:%M:%S.%f"))
            #     try:
            #         robot = RobotDetails.objects.get(name=data["robot"])
            #     except ObjectDoesNotExist:
            #         return Http404("Robot does not exist")
            #     obj, created = RobotLog.objects.get_or_create(robot=robot, node=data["node"],
            #                                                   timestamp=ts, data=data["msg"])
            #     if not created:
            #         print("Log was already in the database")
            # else:
            #     print("\tUser NOT logged in")
            #     return HttpResponseForbidden("User not logged in")
            ts = lx_tz.localize(datetime.strptime(data["timestamp"], "%Y-%m-%d %H:%M:%S.%f"))
            try:
                robot = RobotDetails.objects.get(name="Loop")
            except ObjectDoesNotExist:
                return Http404("Robot does not exist")
            obj, created = RobotLog.objects.get_or_create(robot=robot, node=data["node"], timestamp=ts, data=data["msg"])
            if not created:
                print("Log was already in the database")
        else:
            print("Wrong request method")
    except json.JSONDecodeError as j_err:
        print("Exception decoding {}: {}".format(j_err.doc, j_err.msg))
    except PermissionDenied:
        print("Permission Denied Exception")
        return HttpResponseForbidden("Permission Denied")
    except Exception as err:
        print("Exception occurred: {}".format(err))
        return HttpResponseServerError([err])

    return HttpResponse()