from django.db import models
from django.utils import timezone


class LoopLog(models.Model):
    timestamp = models.DateTimeField(verbose_name="Timestamp", default=timezone.now)
    node = models.CharField(max_length=50, verbose_name="Node", default="")
    data = models.CharField(max_length=500, verbose_name="Data", default="")

    def __str__(self):
        return "Time: {}\nNode: {}\nMessage: {}\n".\
            format(self.timestamp.strftime("%Y-%m-%d %H:%M"), self.node, self.data)

    class Meta:
            indexes = [
                models.Index(fields=['node', 'data'])
            ]
