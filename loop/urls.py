from django.urls import path

from . import views

app_name = 'loop'
urlpatterns = [
    path('', views.LoopList.as_view(), name='overview'),
    path('<pk>/', views.LoopDetail.as_view(), name='loop_detail'),
    path('<pk>/logs', views.LoopLogList.as_view(), name='loop_log_list'),
    path('get_logs', views.get_logs, name="get_logs"),
    path('add_log', views.add_log, name="add_log"),
    path('get_stats', views.get_statistics, name="get_statistics"),
    # path('daily_stats', views.get_daily_statistics, name="get_daily_statistics"),
    path('week_stats', views.get_week_statistics, name="get_week_statistics"),
    # path('month_stats', views.get_month_statistics, name="get_month_statistics"),
]