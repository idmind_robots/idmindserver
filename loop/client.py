#! /usr/bin/env python

import json
import time
import requests
from datetime import datetime

class LogMsg:
    timestamp = None
    node = None         # name of the node given by rospy.get_name()
    level = 'debug'     # Choices are 'debug', 'info', 'warning', 'error'
    msg = ''            # Message to be transmitted

    def __init__(self, node, level, msg):
        self.timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.node = "/idmind_teste"
        self.level = level if level in ['debug', 'info', 'warning', 'error'] else None
        self.msg = msg

    def json(self):
        """ returns a dictionary containing all fields """
        return self.__dict__

    def json_str(self):
        return json.dumps(self.json())

    def __str__(self):
        return '[{}][{}][{}][{}]'.format(self.timestamp, self.node, self.level, self.msg)

idx = 0
while True:
    idx = idx +1
    a = LogMsg(node="/idmind_teste", level='warning', msg="Msg de teste {}".format(idx))
    try:
        print "Posting message"
        data = a.json()
        try:
            requests.post('http://127.0.0.1:8000/loop/add_log', json=data, timeout=5.0)
        except:
            print "Failed to POST to localhost"
        try:
            requests.post('http://192.168.1.68:8000/loop/add_log', json=data, timeout=5.0)
        except:
            print "Failed to POST to raspberry"
        time.sleep(5)
    except Exception as e:
        print 'failed to process loop log request. Reason: %s' % e
        time.sleep(5)
    except KeyboardInterrupt:
        print "\rStopping mock client "
        break

