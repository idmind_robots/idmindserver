import pytz
import requests
from datetime import datetime, timedelta

lx_tz = pytz.timezone("Portugal")
LOCAL_FILE = "/home/cneves/Desktop/gps_boxes.txt"
# SERVER = "http://localhost:8000/loop/add_log"
SERVER = "http://idmind.herokuapp.com/loop/add_log"

# Request all loop logs from local server, day by day
stop_flag = False
page = 0
start = datetime(year=2019, month=3, day=14, hour=0, minute=0, second=0)
stop = datetime(year=2019, month=3, day=14, hour=23, minute=59, second=59)
full_stop = datetime(year=2019, month=9, day=12, hour=10, minute=50, second=38)
params = {"node": nodes}
new_data = False
# while stop < datetime.today():
while stop < full_stop:
    print(start)
    params["loop_username"] = "cneves"
    params["loop_password"] = "Star@Craft2019"
    params["start"] = start.strftime("%d-%m-%Y %H:%M")
    params["stop"] = stop.strftime("%d-%m-%Y %H:%M")
    # Get max pages
    params["page"] = "1"
    r = requests.get(url=LOCAL_SERVER, params=params)
    max_pages = r.json()["max_pages"]
    # Get all logs
    for p in range(1, max_pages+1):
        print("Page "+str(p))
        params["page"] = str(p)
        r = requests.get(url=LOCAL_SERVER, params=params)
        print(r.json())
        log_list = r.json()
        # Write retrieved logs to database
        for log in log_list:
            if log != 'max_pages':
                try:
                    ts = lx_tz.localize(datetime.strptime(log_list[log]["day"]+" "+log_list[log]["time"], "%Y-%m-%d %H:%M:%S.%f"))
                    tstamp = ts.strftime("%Y-%m-%d %H:%M:%S.%f")
                except ValueError:
                    ts = lx_tz.localize(datetime.strptime(log_list[log]["day"] + " " + log_list[log]["time"] + "." + log, "%Y-%m-%d %H:%M:%S.%f"))
                    tstamp = ts.strftime("%Y-%m-%d %H:%M:%S.%f")

                msg_dict = {"robot": "Loop", "timestamp": tstamp, "node": log_list[log]["node"], "msg": log_list[log]["data"]}
                msg_dict["loop_username"] = "palvito_loop"
                msg_dict["loop_password"] = "Star@Craft2019"
                r = requests.post(url=SERVER, json=msg_dict)
                print(r.status_code)
                if r.status_code != 200:
                    print("Status: {} | Content: {}".format(r.status_code, r.content))
                    exit()
                new_data = True

    # Move to the next day
    start = start + timedelta(days=1)
    stop = stop + timedelta(days=1)

