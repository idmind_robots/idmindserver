from django.contrib import admin
from .models import LoopLog


# Register your models here.
class LoopLogAdmin(admin.ModelAdmin):
    fields = ["timestamp", "node", "data"]
    list_display = ("node", "timestamp")


admin.site.register(LoopLog, LoopLogAdmin)
