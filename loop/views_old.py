from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.http import JsonResponse, HttpResponseServerError, HttpResponseNotAllowed
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login
from calendar import monthrange
from .models import LoopLog
from datetime import datetime, timedelta
from math import ceil
import json
import pytz
import time


# Create your views here.
def mainview(request, context={}):
    nodes = LoopLog.objects.order_by('node').values('node').distinct()
    context.update({'loop_nodes': nodes})
    return render(request, 'loop/loop.html', context)


def overview(request):
    lx_tz = pytz.timezone("Portugal")

    day_reboots = 0
    week_reboots = 0
    day_started = 0
    week_started = 0
    day_completed = 0
    week_completed = 0

    for i in range(0,7):
        startday = lx_tz.localize(datetime.today().replace(hour=00, minute=00, second=1)-timedelta(days=i))
        endday = startday.replace(hour=23, minute=59, second=59)
        reboots = LoopLog.objects.filter(timestamp__range=(startday, endday), data="***** Logging is restarting *****").count()
        missions_started = LoopLog.objects.filter(timestamp__range=(startday, endday), data__contains="Mission started").count()
        ph_timeout = LoopLog.objects.filter(timestamp__range=(startday, endday), data__contains="timeout_pharmacy").count()
        missions_completed = LoopLog.objects.filter(timestamp__range=(startday, endday),
                                              data__contains="Mission completed").count()

        if i == 0:
            day_reboots = reboots
            day_started = missions_started - ph_timeout
            day_completed = missions_completed

        week_reboots = week_reboots + reboots
        week_started = week_started + missions_started - ph_timeout
        week_completed = week_completed + missions_completed

    msg_dict = {"day_reboots": day_reboots, "day_started": day_started, "day_completed": day_completed,
                "week_reboots": week_reboots, "week_started": week_started, "week_completed": week_completed}
    print(msg_dict)
    return JsonResponse(msg_dict)


def get_logs(request):
    lx_tz = pytz.timezone("Portugal")
    log_dict = {}
    try:
        if request.method == "GET":
            unodes = request.GET["node"].split("|")
            ustart = lx_tz.localize(datetime.strptime(request.GET["start"], "%d-%m-%Y %H:%M"))
            ustop = lx_tz.localize(datetime.strptime(request.GET["stop"], "%d-%m-%Y %H:%M"))
            page = int(request.GET["page"])
            total_per_page = 25

            log_dict = {}

            logs = LoopLog.objects.\
                filter(node__in=unodes, timestamp__range=(ustart, ustop)).\
                order_by('-timestamp')

            page_logs = logs[(page-1)*total_per_page:page*total_per_page]

            for idx, lg in enumerate(page_logs):
                log_dict.update({str(idx): {"node": lg.node,
                                            "day": lg.timestamp.astimezone(lx_tz).date(),
                                            "time": lg.timestamp.astimezone(lx_tz).time(),
                                            "data": lg.data}})
            log_dict.update({"max_pages": ceil(len(logs)/total_per_page)})

    except Exception as err:
        print("Exception occurred: "+str(err))
        return HttpResponseServerError()
    else:
        return JsonResponse(log_dict)


@csrf_exempt
def add_log(request):
    print("Received a request to add new Loop log")
    lx_tz = pytz.timezone("Portugal")
    try:
        if request.method == "POST":
            data = json.loads(request.body.decode("utf-8", "strict"))
            if "loop_username" in data:
                username = data["loop_username"]
                password = data["loop_password"]
            # Delete as soon as possible # 
            else:
                username = "loop_device"
                password = "loop_password"
            ##############################
            username="idmind"
            password="asdf"
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                ts = lx_tz.localize(datetime.strptime(data["timestamp"], "%Y-%m-%d %H:%M:%S.%f"))
                obj, created = LoopLog.objects.get_or_create(node=data["node"], timestamp=ts, data=data["msg"])
                if not created:
                    print("Log was already in the database")
            else:
                print("\tUser NOT logged in")
                return HttpResponseNotAllowed([])

        else:
            print("Wrong request method")
    except json.JSONDecodeError as j_err:
        print("Exception decoding {}: {}".format(j_err.doc, j_err.msg))
    except PermissionDenied:
        print("Permission Denied Exception")
    except Exception as err:
        print("Exception occured: {}".format(err))

    return HttpResponse()

@csrf_exempt
def get_week_statistics(request):
    lx_tz = pytz.timezone("Portugal")
    log_dict = {}
    try:
        print("Working on Week Statistics")
        if request.method == "GET":
            print(request.GET["start_day"])
            day = int(request.GET["start_day"][0:2])
            month = int(request.GET["start_day"][2:4])
            year = int(request.GET["start_day"][4:])
            naive_start = datetime.today().replace(day=day, month=month, year=year,hour=00, minute=00, second=1)
            missions_started_list = []
            missions_completed_list = []
            reboots_list = []
            for i in range(0, 7):
                start_day = lx_tz.localize(naive_start+timedelta(days=i))
                end_day = lx_tz.localize(naive_start.replace(hour=23, minute=00, second=1)+timedelta(days=i))
                missions_started = LoopLog.objects.filter(timestamp__range=(start_day, end_day), data__contains="Mission started").count()
                missions_completed = LoopLog.objects.filter(timestamp__range=(start_day, end_day), data__contains="Mission completed").count()
                reboots = LoopLog.objects.filter(timestamp__range=(start_day, end_day), data__contains="***** Logging is restarting *****").count()
                missions_started_list.append(missions_started)
                missions_completed_list.append(missions_completed)
                reboots_list.append(reboots)
            log_dict["missions_started"]=missions_started_list
            log_dict["missions_completed"]=missions_completed_list
            log_dict["reboots"]=reboots_list
        else:
            print("No information provided on GET")
    except Exception as err:
        print("Exception occurred: " + str(err))
        return HttpResponseServerError()
    else:
        return JsonResponse(log_dict)

@csrf_exempt
def get_month_statistics(request):
    lx_tz = pytz.timezone("Portugal")
    log_dict = {}
    try:
        print("Working on Month Statistics")
        if request.method == "GET":
            month = int(request.GET["start_day"][2:4])
            year = int(request.GET["start_day"][4:])
            start_day = lx_tz.localize(datetime.today().replace(day=1, month=month, year=year, hour=00, minute=00, second=1))
            end_day = lx_tz.localize(datetime.today().replace(day=monthrange(year, month)[1], month=month, year=year, hour=00, minute=00, second=1))

            missions_started = LoopLog.objects.filter(timestamp__range=(start_day, end_day), data__contains="Mission started").count()
            missions_completed = LoopLog.objects.filter(timestamp__range=(start_day, end_day), data__contains="Mission completed").count()
            reboots = LoopLog.objects.filter(timestamp__range=(start_day, end_day), data__contains="***** Logging is restarting *****").count()

            log_dict["missions_started"]=missions_started
            log_dict["missions_completed"]=missions_completed
            log_dict["reboots"]=reboots
        else:
            print("No information provided on GET")
    except Exception as err:
        print("Exception occurred: " + str(err))
        return HttpResponseServerError()
    else:
        return JsonResponse(log_dict)
