function toggle_menu(menu){
    if(menu.id=="navbar_projects"){
        // Toggle 'active' state
        var deactivate = menu.classList.contains('active');
        if(deactivate)
            menu.classList.remove('active');
        else
            menu.classList.add('active');

        var entries = document.getElementsByClassName('navbar_projects_button');
        for(var idx=0; idx < entries.length; idx++){
            if(deactivate){
                entries[idx].style.display = 'none';
            }
            else
                entries[idx].style.display = 'block';
        }
    }
    else if(menu.id=="navbar_models"){
        // Toggle 'active' state
        var deactivate = menu.classList.contains('active');
        if(deactivate)
            menu.classList.remove('active');
        else
            menu.classList.add('active');

        var entries = document.getElementsByClassName('navbar_models_button');
        for(var idx=0; idx < entries.length; idx++){
            if(deactivate){
                entries[idx].style.display = 'none';
            }
            else
                entries[idx].style.display = 'block';
        }

    }
}