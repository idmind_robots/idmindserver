from django.urls import path

from . import views

app_name = 'sugal'
urlpatterns = [
    path('', views.GiraListView.as_view(), name='gira_listview'),
    path('bike', views.add_log, name='gira_addlog'),
]