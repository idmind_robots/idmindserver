import re
import pytz
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView, CreateView, UpdateView

lx_tz = pytz.timezone("Portugal")


# Create your views here.
class GiraListView(LoginRequiredMixin, ListView):
    login_url = '/accounts/login'
    template_name = "gira/gira_list.html"
    context_object_name = "gira_devices"
    paginate_by = 20


@csrf_exempt
def add_log(request):
    return HttpResponse(status=200)
