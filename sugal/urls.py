from django.urls import path

from . import views

app_name = 'sugal'
urlpatterns = [
    path('', views.GPSListView.as_view(), name='sugal_listview'),                          # This shows the table of GPS, ordered by box_nr ##NEEDS TO BE CHANGED
    path('detail/<pk>', views.GPSDetailView.as_view(), name='sugal_detailview'),
    path('get_gps_list', views.get_gps_list, name='sugal_gpslist'),
    path('update_warnings/', views.update_warnings, name='sugal_warnings'),             # This should be called periodically to update warning messages
    path('detail/<imei>/get_bat_logs', views.get_gps_bat_logs, name="sugal_getbatlogs"),                   # Returns the logs of a given GPS (to update battery plots)
    path('detail/<imei>/get_gps_logs', views.get_gps_logs, name="sugal_getgpslogs"),                   # Returns the logs of a given GPS (to update battery plots)
    path('detail/<imei>/full_logs', views.GPSLogList.as_view(), name="sugal_fulllogs"),                   # Returns the logs of a given GPS (to update battery plots)
    # path('create_gps/',views.GPSCreate.as_view(), name="sugal_create_gps"),             # Returns the view to create a new GPS
    path('edit_gps/<int:pk>', views.GPSUpdate.as_view(), name="sugal_edit_gps"),         # Returns the view to edit a GPS
    path('add_log/', views.add_log, name='sugal_addlog'),                               # Function that adds a log to the database
    path('add_multiple_log/', views.add_multiple_log, name='sugal_addmultilog'),                               # Function that adds multiple logs to the database
]