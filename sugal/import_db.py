import os
import json
import requests
import datetime

LOGS_DIR = "/home/cneves/gps_logs/proxy_logs/"
# SERVER = "http://localhost:8000/sugal/add_log/"
SERVER = "http://idmind.herokuapp.com/sugal/add_log/"


def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


for server in ['pt', 'es']: #, 'qa']:
    print("=== SERVER: {} ===".format(server))
    for f in sorted(os.listdir(LOGS_DIR+server), reverse=True):
        print(f)
        with open(LOGS_DIR+server+"/"+f, 'r') as log:
            for log_line in log:
                # log_line = log.read()
                start = log_line.find('{')
                stop = log_line.find('}')+1
                # print(log_line[start:stop])
                msg_dict = json.loads(log_line[start:stop])
                msg_dict["server"] = server
                day = f[:-4].replace('_', '')
                time = log_line[:8].replace(':', '')
                # print(day+time)
                msg_dict["timestamp_received"] = day+time
                r = requests.post(SERVER, json=msg_dict)
                if r.status_code != 201:
                    print("{} - {}".format(r.status_code, r.content))
                    # input("Press Enter to continue...")
