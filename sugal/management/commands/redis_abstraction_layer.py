#! /usr/bin/env python3


""" This Module defines the Data transfer objects for GPS locations and exports methods for storing

		data, as well as querying it """

import redis, json
from datetime import datetime


def _connect():
	return redis.StrictRedis(host='localhost', port=6379, db=0)


class LocationDTO(object):
	r = _connect()
	def __init__(self, device_id):
		self.device_id = device_id
		self.timestamp = None
		self.valid = False
		self.latitude = 0.0
		self.longitude = 0.0
		self.velocity = 0.0
		self.battery = None
		self.ext_battery = None

	def store(self):
		print('storing: {}'.format(self.__dict__))
		key = 'location:%s' % self.device_id
		score = float(self.timestamp)
		value = json.dumps(self.__dict__)
		self.r.zadd(key, score, value)


class DeviceDTO(object):
	def __init__(self, serial, imei, phone, rate, box=None):
		self.serial = serial
		self.imei = imei
		self.phone = phone
		self.rate = rate
		self.box = box

	def store(self):
		r = _connect()
		r.sadd('devices', json.dumps(self.__dict__))


def get_battery_history(device_id, day):
	""" Returns a list of location objects, 
		
		for the given device, at the given timeframe, ordered by timestamp """
	r = _connect()
	data = list(r.zrangebyscore('location:%s' % device_id, day + '000000', day + '235959'))
	return map(json.loads, data)


def get_last_location(device_id):
	r = _connect()
	data = r.zrange('location:' + str(device_id), -1, -1)
	return None if not len(data) else json.loads(data[0])


def get_devices():
	""" Returns a list of registered devices.
		If type is specified, returns only the subset of the given type """
	r = _connect()
	data_raw = list(r.smembers('devices'))
	return map(json.loads, data_raw)

def unregister_device(imei):
	""" Unregisters a device. All location messages are maintained """
	r = _connect()
	devices = r.smembers('devices')
	to_remove = ''
	for d in devices:
		if json.loads(d)['imei'] == imei:
			to_remove = d
	if to_remove:
		r.srem('devices', to_remove)


def update_device(imei, data):
	"""  Updates the device <imei> with de fields defined in the data dictionary """
	def update_device_impl(pipe):
		devices = map(json.loads, list(pipe.smembers('devices')))
		device = None
		for d in devices:
			if d["imei"] == imei:
				device = d
		if not device:
			raise Exception('device not found')
		pipe.multi()
		pipe.srem('devices', json.dumps(device))
		for k in data:
			device[k] = data[k]
		pipe.sadd('devices', json.dumps(device))
		
	r = _connect()
	return r.transaction(update_device_impl, 'devices')
