#! /usr/bin/env python3

import json
import pytz
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist
from sugal.models import GPSDevices, GPSLogs


class Command(BaseCommand):

    help = 'Scans all GPS devices and signals yellow and red warnings'

    def handle(self, *args, **options):
        lx_tz = pytz.timezone("Portugal")
        gps_list = GPSDevices.objects.all()

        # Check every registered device
        for gps in gps_list:
            print("Analyzing GPS {}".format(gps.imei))

            # Get Last Log or ignore, must be a new device
            try:
                last_log = GPSLogs.objects.filter(gps__imei=gps.imei).latest('timestamp')
            except ObjectDoesNotExist:
                print("GPS {} does not have logs yet.".format(gps.imei))
                continue

            # If last log exists, check how much time passed
            try:
                if lx_tz.localize(datetime.now()) - last_log.timestamp > timedelta(hours=24):
                    print("No log for over 24h. Turning Yellow.")
                    if "No log for over 24h." not in gps.comments:
                        gps.comments = gps.comments + "No log for over 24h. "
                    if gps.warning < 1:
                        gps.warning = 1
            except Exception as log_err:
                print("Exception checking last log timestamp: {}".format(log_err))

            # If last log exists, check external battery
            try:
                if last_log.ext_battery < 3.9:
                    print("External Battery low. Turning Red.")
                    if "External Battery low." not in gps.comments:
                        gps.comments = gps.comments + "External Battery low. "
                    gps.warning = 2
            except Exception as log_err:
                print("Exception checking last log external battery: {}".format(log_err))

            # If last log exists, check internal battery
            try:
                if last_log.battery < 3.9:
                    print("Internal Battery low. Turning Red.")
                    if "Battery low." not in gps.comments:
                        gps.comments = gps.comments + "Battery Low. "
                    gps.warning = 2
            except Exception as log_err:
                print("Exception checking last log internal battery: {}".format(log_err))

            # Search for last log marked as Sleep
            try:
                last_sleep = GPSLogs.objects.filter(gps__imei=gps.imei, valid="S").latest('timestamp')
                if lx_tz.localize(datetime.now()) - last_sleep.timestamp > timedelta(hours=24):
                    print("No Sleep for over 24h. Turning Yellow.")
                    if "No sleep for over 24h." not in gps.comments:
                        gps.comments = gps.comments + "No sleep for over 24h."
                    if gps.warning < 1:
                        gps.warning = 1
            except ObjectDoesNotExist:
                print("Device has never slept, must be new")
            except Exception as log_err:
                print("Exception checking last sleep log: {}".format(log_err))

            # Save changes made to the GPS warning and comment fields
            print("Saving {} with warning {}".format(gps, gps.warning))
            gps.save()