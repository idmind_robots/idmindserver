#! /usr/bin/env python3

import json
import redis
from .redis_abstraction_layer3 import get_devices
from django.core.management.base import BaseCommand, CommandError
from sugal.models import GPSDevices


class Command(BaseCommand):

    help = 'Loads the data from redis database'

    def handle(self, *args, **options):

        try:
            dev_list = get_devices()
        except redis.exceptions.ResponseError as red_err:
            print("Unable to connect to database: {}".format(red_err))
            return
        except Exception as db_err:
            print("Unable to get list of devices: {}".format(db_err))
            return

        gps_fails = []
        saved = 0
        #print("Loading {} devices.".format(len(dev_list)))
        print("Loading devices.")
        for gps in dev_list:
            try:
                (gps_dev, created) = GPSDevices.objects.get_or_create(id=gps["imei"])
                print("New GPS instance created" if created else "GPS loaded")
                gps_dev.serial_number = gps["serial"]
                gps_dev.phone_number = gps["phone"]
                gps_dev.box_nr = gps["box"]
                gps_dev.com_rate = gps["rate"]
                gps_dev.save()
                saved = saved + 1
                print("Saved new info on GPS {}".format(gps["imei"]))
            except Exception as save_err:
                print("Failed to save info on GPS {}: {}".format(gps["imei"], save_err))
                gps_fails.append(gps["imei"])

        print("Saved {} devices.".format(saved-len(gps_fails)))
        print("Failed to save:")
        for dev in gps_fails:
            print("\t{}".format(dev))

