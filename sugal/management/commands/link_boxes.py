#! /usr/bin/env python
from django.core.management.base import BaseCommand
from idmindserver.sugal.models import GPSDevices
from datetime import datetime
import pytz


class Command(BaseCommand):

    def handle(self, *args, **options):
        pt_tz = pytz.timezone("Portugal")
        logs = GPSLogs.objects.all()

        for l in logs:
            l.timestamp = pt_tz.localize(datetime.combine(l.day2, l.timestamp2))
            print("Original: {} and {}".format(l.day2, l.timestamp2))
            print("Result: {}".format(l.timestamp))
            l.save()

