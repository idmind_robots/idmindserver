#!/usr/bin/env python

import requests
import datetime

URL = 'http://127.0.0.1:8000/sugal/add_log/'

# client = requests.session()
# client.get(URL)
# for c in client.cookies:
#     print(c)
#
# if 'csrftoken' in client.cookies:
#     csrftoken = client.cookies['csrftoken']
# else:
#     csrftoken = client.cookies['csrf']

# data = dict(username="gps_device", password="gps_password")

#AAA,34,(-)Latitude,(-)Longitude,Date and time,Positioning status,Number of satellites,GSM signal strength,Speed,Direction,HDOP,Altitude,Mileage,Run time,Base station info,I/O port status,Analog input value
# Consider randomizing stuff
data = dict()
data["device_id"] = "351302070212534"
data["timestamp"] = 180214150814
data["latitude"] = 38.769795
data["longitude"] = -9.179470
data["valid"] = "V"
data["velocity"] = "0"
data["battery"] = 4.2
data["ext_battery"] = 4.9
data["raw"] = "$$G148,357302070212534,AAA,35,38.769795,-9.179470,180214150814,V,0,22,0,0,0.0,0,231,20863,268|1|0002|015C5C84,0000,0001|0001|001D|019A|01DF,00000001,*B7\r\n"

r = requests.post(URL, json=data)
print(r)

######
## JSON "device_id": float, "timestamp": int, "valid": bool, "latitude": float, "longitude": float, "velocity": float, "battery": float, "ext_battery": float, "raw": string
##
##