from django.apps import AppConfig


class SugalConfig(AppConfig):
    name = 'sugal'
