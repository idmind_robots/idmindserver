from django.db import models
from django.forms import ModelForm
from django.core.validators import RegexValidator
from django.utils import timezone
from datetime import datetime


class GPSDevices(models.Model):
    """ GPS Device model
    Has an ID (imei), serial number, model (in case we need to change), owner (in case of multiple projects), phone number and validator
    communication rate when it is sleeping, box number and if is active or not
    Also has the is_active() method and __str__()
    """
    model = models.CharField(max_length=100, verbose_name="Model", choices=(("M", "Meitrack"), ("T", "TK")),
                             default="M")
    imei_regex = RegexValidator(regex=r'^\d{1,20}$', message="Enter a valid IMEI (1-20 digits)")
    imei = models.CharField(max_length=20, verbose_name="IMEI", primary_key=True, default="0000000000")
    serial_number = models.CharField(max_length=200, verbose_name="Serial Number", default="0000000000")
    owner = models.CharField(max_length=100, verbose_name="Owner", choices=(("IDMind", "IDMind"),
                                     ("FocusBC", "FocusBC"), ("SUGAL", "SUGAL"), ("O", "Other")), default="IDMind")
    server = models.CharField(max_length=100, verbose_name="Server", choices=(("PROD PT", "PROD PT"),
                                     ("PROD ES", "PROD ES"), ("QA", "QA"), ("O", "Other")), default="PROD PT")
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(verbose_name="Phone Nr.", validators=[phone_regex], max_length=13, blank=True, default="+000000000000")  # validators should be a list
    box_regex = RegexValidator(regex=r'^\d{3,4}', message="Enter a valid 3-4 digit box number")
    box_nr = models.CharField(max_length=10, verbose_name="Box", default="0000", validators=[box_regex])
    # Accomodate for portuguese license and both formats of spanish license plate
    license_regex = RegexValidator(regex = r'([A-Z0-9]{2}-[A-Z0-9]{2}-[A-Z0-9]{2})|([0-9]{4}[A-Z]{3})|([A-Z]{3}-[0-9]{4}-[A-Z]{2})$', message="Enter a valid license plate XX-XX-XX, 0000XXX or XXX0000XX")
    license_plate = models.CharField(max_length=15, verbose_name="License Plate", default="00-AA-00", validators=[license_regex])
    state = models.CharField(max_length=20, verbose_name="State", choices=(("Active", "Active"),
                                     ("Backup", "Backup"),  ("Inactive", "Inactive")), default="Active")
    charging = models.BooleanField(verbose_name="Charging", default=False)
    warning = models.IntegerField(verbose_name="Warnings", default=0)
    comments = models.CharField(max_length=500, verbose_name="Comments", default="", blank=True)
    last_check = models.DateTimeField(verbose_name="Last Checked", default=timezone.now)

    def __str__(self):
        return str(self.model) + "-" + str(self.imei)

    def as_dict(self):
        #return {"imei": self.imei, "serial_number": self.serial_number, "model": self.model, "owner": self.owner,
        #        "phone_number": self.phone_number, "license_plate": self.license_plate, "box_nr": self.box_nr, "state": self.state}
        return [(field.verbose_name, field.value_to_string(self)) for field in GPSDevices._meta.fields]

    class Meta:
        ordering = ['imei', 'box_nr']


class GPSLogs(models.Model):
    """ GPS Logs model
    Each GPS send logs, that are stored into the database
    The collected data is the timestamp, position, speed, validity/state of the GPS, internal battery and external battery
    """
    gps = models.ForeignKey(GPSDevices, on_delete=models.CASCADE)
    timestamp_received = models.DateTimeField(verbose_name="Rcv. Timestamp", default=timezone.now)
    timestamp_msg = models.DateTimeField(verbose_name="Msg. Timestamp", default=timezone.now)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, verbose_name="Latitude")
    longitude = models.DecimalField(max_digits=9, decimal_places=6, verbose_name="Longitude")
    event = models.IntegerField(verbose_name="Last Message Event", default=0)
    speed = models.FloatField(verbose_name="Speed")
    valid = models.CharField(max_length=2, verbose_name="Validity", choices=(("V", "Valid"), ("I", "Invalid"), ("H", "Heartbeat"), ("S", "Sleep"), ("A", "Awake")))
    battery = models.FloatField(verbose_name="Internal Battery")
    ext_battery = models.FloatField(verbose_name="External Battery")
    raw = models.CharField(max_length=500, verbose_name="Raw Msg", default="")

    def __str__(self):
        return self.gps.__str__()+" "+self.timestamp_msg.strftime("%d-%m-%y %H:%M:%S")

    class Meta:
        ordering = ['timestamp_msg']


class NewGPSForm(ModelForm):
    class Meta:
        model = GPSDevices
        fields = ['imei', 'model', 'owner', 'phone_number']

