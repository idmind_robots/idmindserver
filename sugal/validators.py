from django.core.validators import RegexValidator


class LicensePlateValidator(RegexValidator):
    regex = r'^$'