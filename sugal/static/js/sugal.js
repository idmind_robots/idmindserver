/***********************/
/* Auxiliary functions */
/***********************/
function httpGetAsync(theUrl, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous
    xmlHttp.send(null);
}

function convert_date(fullDate, type){
    day = fullDate.getDate();
    if(fullDate.getDate() < 10)
        day = "0" + day;
    month = fullDate.getMonth()+1;
    if(fullDate.getMonth()+1 < 10)
        month = "0" + month;
    year = fullDate.getFullYear();

    if(type=="dmy")
        return day+"-"+month+"-"+year
    if(type=="ymd")
        return year+"-"+month+"-"+day
}
/*************/
/* GPS List  */
/*************/
function request_gps_list(order){
    // Get filters and order
    box_nr = document.getElementById("box_filter").value
    imei = document.getElementById("imei_filter").value
    server = document.getElementById("server_filter").value
    charging_true = document.getElementById("charging_filter_yes").checked
    charging_false = document.getElementById("charging_filter_no").checked
    // Get data (filtered and ordered)
    link = "/sugal/get_gps_list?order_by="+order

    if(box_nr!='')
        link += "&box_nr="+box_nr
    if(imei!='')
        link += "&imei="+imei
    if(server!='All')
        link += "&server="+server
    if(!(charging_true == charging_false)){
        link += "&charging="+(charging_true ? true : false)
    }
    httpGetAsync(link, update_gps_table)
}

function update_gps_table(response){
    resp = JSON.parse(response.responseText);

    // Get table and remove tbody
    var tb = document.getElementById("device_list_table");
    tb.removeChild(tb.getElementsByTagName("tbody")[0]);

    //Create new tbody
    var tbd = document.createElement("tbody");
    var gps_count = 0;
    var fields = ["box_nr", "imei", "server", "charging"];
    for (g in resp){
        gps_count +=1;
        var row = document.createElement("tr");
        for (f in fields){
            var td = document.createElement("td");
            td.innerHTML = resp[g][fields[f]];
            row.appendChild(td);
        }
        // Warning
        var td = document.createElement("td");
        light = (resp[g]['warning'] == 0) ? 'green' : ((resp[g]['warning'] == 1) ? 'yellow' : 'red');
        td.innerHTML = "<a href='detail/"+resp[g]['imei']+"'><img src='/static/img/"+light+"_light.jpg' height='15px' /></a>";
        row.appendChild(td);
        tbd.appendChild(row);
    }
    tb.appendChild(tbd);
    document.getElementById("gps_count").innerHTML ="<p>Showing "+gps_count+" devices</p>";
}

/***************/
/* GPS Detail  */
/***************/
function select_tab(evt, tab_name){
    var tabs = document.getElementsByClassName("tabcontent");
    var tab_buttons = document.getElementsByClassName("tab_button");

    for (i=0; i < tabs.length; i++){
        tabs[i].style.display = "none";
    }
    document.getElementById(tab_name).style.display = "flex";

    for (i=0; i < tab_buttons.length; i++){
        tab_buttons[i].id = "";
    }
    evt.currentTarget.id="open";

}

function update_plot_data(){
    date = document.getElementById("log_calendar").value
    request_plot_data(new Date(date));
}

function request_plot_data(plotDate){
    d = convert_date(plotDate, "dmy");
    imei = document.getElementById("detail_imei").value
    httpGetAsync("/sugal/detail/"+imei+"/get_bat_logs?day="+d, display_plot)
}

function display_plot(response){
    console.log(response.responseText)
    resp = JSON.parse(response.responseText);

    start = new Date(document.getElementById("log_calendar").value).setHours(0,0,1)
    end = new Date(document.getElementById("log_calendar").value).setHours(23,59,59)

    var ctx = document.getElementById("gpslog_chart").getContext('2d');
    var data = {
        labels: [],
        datasets: [{
            label: "Internal Battery",
            data: [],
            backgroundColor: 'rgba(0, 255, 0, 1)',
        },
        {
            label: "External Battery",
            data: [],
            backgroundColor: 'rgba(0, 0,255, 1)',
        }],
    };
    var options = {
        title: {
            display: true,
            text: document.getElementById("log_calendar").value,
        },
        scales: {
            yAxes: [{
                ticks: {
                    min: 0.0,
                    max: 6.0,
                    beginAtZero:true,
                },
                scaleLabel: {
                    display: true,
                    labelString: "Voltage",
                }
            }],
            xAxes:[{
                type: 'time',
                time: {
                    unit: 'hour',
                    displayFormats: {hour: 'hA'},
                    min: start,
                    max: end,
                },
                offset: true,
                scaleLabel: {
                    display: true,
                    labelString: "Time Of Day",
                }
            }]
        }
    };
    var config = {
        type: 'line',
        data: data,
        options: options
    };

    for(l in resp){
        data.labels.push(resp[l][0])
        data.datasets[0].data.push(resp[l][1])
        data.datasets[1].data.push(resp[l][2])
    }

    var myChart = new Chart(ctx, config);
}

/***********************************/
/*         LOGS TABLE              */
/***********************************/
function request_gps_logs(order, qtd){
    // Get filters and order
//    box_nr = document.getElementById("box_filter").value
    imei = document.getElementById("detail_imei").value
//    server = document.getElementById("server_filter").value
//    charging_true = document.getElementById("charging_filter_yes").checked
//    charging_false = document.getElementById("charging_filter_no").checked

    // Get data (filtered and ordered)
    console.log(order)
    link = "/sugal/detail/"+imei+"/get_gps_logs?order_by="+order+"&limit="+qtd
    httpGetAsync(link, update_logs_table)
}

function update_logs_table(response){
    resp = JSON.parse(response.responseText);
    console.log(resp)
    // Get table and remove tbody
    var tb = document.getElementById("log_list_table");
    tb.removeChild(tb.getElementsByTagName("tbody")[0]);

    //Create new tbody
    var tbd = document.createElement("tbody");
    for (g in resp){
        var row = document.createElement("tr");
        fields = ["timestamp_received", "timestamp_msg", "latitude", "longitude", "event", "speed", "valid", "battery", "ext_battery"];
        for (f in fields){
            var td = document.createElement("td");
            td.innerHTML = resp[g][fields[f]];
            row.appendChild(td);
        }
        tbd.appendChild(row);
//        THIS CODE WILL ADD THE RAW MESSAGE TO A NEW LINE IN THE TABLE
//        var row = document.createElement("tr");
//        var td = document.createElement("td");
//        td.colSpan = 9;
//        td.innerHTML = resp[g]['raw']+" | https://www.w3schools.com/css/css_tooltip.asp";
//        row.appendChild(td);
//        tbd.appendChild(row);
    }
    tb.appendChild(tbd);
}

/*************************
LOGS
************************/
function set_form_defaults(){
    /* Called on page load, to set the values of the form, if any were set */
    set_calendar_values();

    // Check if event is in the GET params. If so, choose as default. Otherwise, all is already selected.
    if(window.location.href.includes("?")){
        var params = window.location.href.split("?");
        var param_list = params[1].split("&");
        for(p in param_list){
            if(param_list[p].includes("log_event")){
                var droplist = document.getElementById("log_event");
                var default_option = param_list[p].split("=")[1];
                for(opt in droplist.options){
                    if(droplist.options[opt].value == default_option){
                        droplist.options[opt].selected = "selected";
                    }
                    else{
                        droplist.options[opt].selected = "";
                    }
                }
            }
            if(param_list[p].includes("log_valid")){
                var droplist = document.getElementById("log_valid");
                var default_option = param_list[p].split("=")[1];
                for(opt in droplist.options){
                    if(droplist.options[opt].value == default_option){
                        droplist.options[opt].selected = "selected";
                    }
                    else{
                        droplist.options[opt].selected = "";
                    }
                }
            }
        }
    }
}

function set_calendar_values(){
    // Generate default values for start and stop
    var now = new Date();
    var year = now.getFullYear();
    var month = now.getMonth()+1;
    var day = now.getDate();
    var start_str = year + "-" + (month < 10 ? "0" + month.toString() : month) + "-" +
                    (day < 10 ? "0" + day.toString() : day) + "T" + "00:00";
    var end_str = year + "-" + (month < 10 ? "0" + month.toString() : month) + "-" +
                    (day < 10 ? "0" + day.toString() : day) + "T" + "23:59";

    if(window.location.href.includes("?")){
        var params = window.location.href.split("?");
        var param_list = params[1].split("&");
        for(p in param_list){
            if(param_list[p].includes('start')){
                var aux = param_list[p].split('=')[1];
                if(aux.length > 1){
                    start_str = aux;
                }
            }
            if(param_list[p].includes('stop')){
                var aux = param_list[p].split('=')[1];
                if(aux.length > 1){
                    end_str = aux;
                }
            }
        }
    }
    start_str = start_str.replace("%3A", ":");
    end_str = end_str.replace("%3A", ":");
    // Assign values to input elements
    document.getElementById('start_input').value = start_str;
    document.getElementById('stop_input').value = end_str;
}

function change_page(page){
    console.log("Changing Sugal logs page")
    curr_location = window.location.href;
    if(curr_location.includes('page')){
        // Deconstruct the URL
        var parts = curr_location.split("?");
        var fields = parts[1].split("&");
        for(f in fields){
            if(fields[f].includes('page')){
                var page_parts = fields[f].split("=")
                var page_idx = f;
            }
        }
        // Reconstruct the URL
        var new_url = parts[0]+"?";
        for(f in fields){
            if(f != 0) new_url = new_url + "&";
            if(f == page_idx){
                new_url = new_url + page_parts[0]+"="+page;
            }
            else{
                new_url = new_url + fields[f];
            }
        }
        console.log(new_url);
        window.location.href = new_url;
    }
    else{
        if(curr_location.includes("?")){
            console.log(curr_location + "&page="+page);
            window.location.href = curr_location + "&page="+page;
        }
        else{
            console.log(curr_location + "?page="+page);
            window.location.href = curr_location + "?page="+page;
        }
    }

}