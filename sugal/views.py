from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from datetime import datetime, date, time, timedelta
from .models import GPSDevices, GPSLogs
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
import json
import pytz
import re

valid = {"V": "Valid", "I": "Invalid", "H": "Heartbeat", "S": "Sleep", "A": "Awake"}
lx_tz = pytz.timezone("Portugal")


class GPSListView(LoginRequiredMixin, ListView):
    login_url = '/accounts/login'
    model = GPSDevices
    template_name = "sugal/gps_list.html"
    context_object_name = "gps_devices"
    paginate_by = 20

    def get_ordering(self):
        print(self.kwargs)
        if not self.kwargs:
            ordering = 'imei'
        else:
            ordering = self.kwargs['pk']
        return ordering


class GPSDetailView(LoginRequiredMixin, DetailView):
    login_url = '/accounts/login'
    model = GPSDevices
    context_object_name = "gps_detail"
    template_name = 'sugal/gps_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['gps_list'] = GPSDevices.objects.order_by('box_nr', 'imei')
        return context


class GPSLogList(LoginRequiredMixin, ListView):
    login_url = '/accounts/login'
    model = GPSLogs
    template_name = 'sugal/gps_logs.html'
    context_object_name = "gps_logs"
    paginate_by = 20

    def get_queryset(self):

        # Apply time filters
        try:
            start_time = lx_tz.localize(datetime.strptime(self.request.GET["start"], "%Y-%m-%dT%H:%M"))
        except KeyError:
            start_time = lx_tz.localize(datetime.combine(date.today(), time()))
        try:
            stop_time = lx_tz.localize(datetime.strptime(self.request.GET["stop"], "%Y-%m-%dT%H:%M"))
        except KeyError:
            stop_time = lx_tz.localize(datetime.combine(date.today(), time.max))
        logs = GPSLogs.objects.filter(gps__imei=self.kwargs['imei'], timestamp_received__range=(start_time, stop_time)).order_by('timestamp_received')

        # Apply event filter
        try:
            ev = self.request.GET["log_event"]
            if ev.isnumeric():
                logs = logs.filter(event=int(ev))
        except KeyError:
            print("No event was specified")

        # Apply validity filter
        try:
            valid = self.request.GET["log_valid"]
            if valid != 'all':
                logs = logs.filter(valid=valid)
        except KeyError:
            print("No validity label was specified")

        return logs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['gps_detail'] = GPSDevices.objects.get(imei=self.kwargs['imei'])
        context['gps_events'] = GPSLogs.objects.order_by().values('event').distinct()
        context['gps_valid'] = GPSLogs.objects.order_by().values('valid').distinct()
        return context


class GPSCreate(LoginRequiredMixin, CreateView):
    login_url = '/accounts/login'
    model = GPSDevices
    template_name = 'sugal/gps_form.html'


class GPSUpdate(LoginRequiredMixin, UpdateView):
    login_url = '/accounts/login'
    model = GPSDevices
    template_name = 'sugal/gps_form.html'


@login_required
def get_gps_list(request):
    order = request.GET['order_by']
    gps_list = GPSDevices.objects.all().order_by(order)
    try:
        box = request.GET['box_nr']
        gps_list = gps_list.filter(box_nr__icontains=box)
    except KeyError:
        print("There are no box_nr filters")
    try:
        imei = request.GET['imei']
        gps_list = gps_list.filter(imei__icontains=imei)
    except KeyError:
        print("There are no imei filters")
    try:
        charging = (request.GET['charging'] == 'true')
        gps_list = gps_list.filter(charging=charging)
    except KeyError:
        print("There are no charging filters")
    try:
        server = request.GET['server']
        gps_list = gps_list.filter(server=server)
    except KeyError:
        print("There are no server filters")

    gps_dict = {}
    for g in gps_list:
        gps_dict.update({str(g): {
            "model": g.model,
            "box_nr": g.box_nr,
            "imei": g.imei,
            "serial_number": g.serial_number,
            "owner": g.owner,
            "server": g.server,
            "phone_number": g.phone_number,
            "license_plate": g.license_plate,
            "state": g.state,
            "charging": g.charging,
            "warning": g.warning,
        }})
    return JsonResponse(gps_dict)


@login_required
def get_gps_bat_logs(request, imei):
    """
    Returns the gps logs of a given GPS as JSON (for displaying in chart)
    :param request:
    :param imei:
    :return:
    """
    response = {}
    if request.method == "GET":
        # imei = request.GET["imei"]
        day = lx_tz.localize(datetime.strptime(request.GET["day"], "%d-%m-%Y"))
        startday = day.replace(hour=0, minute=0, second=0)
        endday = day.replace(hour=23, minute=59, second=59)
        logs = GPSLogs.objects.filter(gps__imei=imei, timestamp_msg__range=(startday, endday)).order_by('timestamp_msg')
        for idx, lg in enumerate(logs):
            response[str(idx)] = (lg.timestamp_msg.astimezone(lx_tz), lg.battery, lg.ext_battery)
        return JsonResponse(response)
    else:
        return JsonResponse(response)


@login_required
def get_gps_logs(request, imei):
    """
    Returns the gps logs of a given GPS as JSON (for displaying in chart)
    :param request:
    :param imei:
    :return:
    """
    response = {}
    if request.method == "GET":
        # order = request.GET.get('order', 'timestamp_msg')
        order = request.GET.get('order_by')
        qtd = int(request.GET.get('limit'))
        logs = GPSLogs.objects.filter(gps__imei=imei).order_by(order)[0:qtd]
        for idx, lg in enumerate(logs):
            response.update({str(idx): {
                "timestamp_received": lg.timestamp_received,
                "timestamp_msg": lg.timestamp_msg,
                "latitude": lg.latitude,
                "longitude": lg.longitude,
                "event": lg.event,
                "speed": lg.speed,
                "valid": lg.valid,
                "battery": lg.battery,
                "ext_battery": lg.ext_battery,
                "raw": lg.raw,
            }})
        return JsonResponse(response)
    else:
        return JsonResponse(response)


@csrf_exempt
def add_log(request):
    print("Received a new log "),
    if request.method == "POST":
        try:
            data = json.loads(request.body.decode("utf-8", "strict"))
            try:
                gps_imei = data["id"]
                gps_model = "M"
                gps_dev = GPSDevices.objects.get(imei=gps_imei, model=gps_model)
            except ObjectDoesNotExist:
                gps_dev = GPSDevices()
                gps_dev.imei = data["id"]
                gps_dev.model = "M"
                if data["server"] == "pt":
                    gps_dev.server = "PROD PT"
                elif data["server"] == "es":
                    gps_dev.server = "PROD ES"
                elif data["server"] == "qa":
                    gps_dev.server = "QA"
                else:
                    gps_dev.server = data["server"]
                gps_dev.warning = 1
                gps_dev.comments = "Created automatically"
                gps_dev.save()
                print("New GPS created")
            print(gps_dev)
            try:
                try:
                    # Check if log already exists
                    raw_msg = msg_parser(data["message_raw"])
                    ts = pytz.timezone("Europe/Lisbon").localize(
                        datetime.strptime(str(raw_msg["timestamp"]), "%y%m%d%H%M%S"))
                    tr = pytz.timezone("Europe/Lisbon").localize(
                        datetime.strptime(str(data["timestamp"]), "%y%m%d%H%M%S"))
                    gps_log = GPSLogs.objects.get(gps=gps_dev, timestamp_msg=ts, timestamp_received=tr)
                    print("Log already exists")
                    return HttpResponse(status=409)
                except MultipleObjectsReturned:
                    print("Log already exists... twice...")
                    return HttpResponse(status=409)
                except ObjectDoesNotExist:
                    print("Saving log...")
                    gps_log = GPSLogs()
                    gps_log.gps = gps_dev
                    ts = pytz.timezone("Europe/Lisbon").localize(
                        datetime.strptime(str(raw_msg["timestamp"]), "%y%m%d%H%M%S"))
                    gps_log.timestamp_msg = ts
                    if "timestamp_received" in data.keys():
                        ts = pytz.timezone("Europe/Lisbon").localize(
                            datetime.strptime(str(data["timestamp_received"]), "%y%m%d%H%M%S"))
                    else:
                        ts = pytz.timezone("Europe/Lisbon").localize(datetime.now())
                    gps_log.timestamp_received = ts
                    gps_log.latitude = data["latitude"]
                    gps_log.longitude = data["longitude"]
                    gps_log.event = raw_msg["event"]
                    gps_log.speed = data["velocity"]
                    gps_log.valid = data["valid"]
                    gps_log.battery = data["battery"]
                    gps_log.ext_battery = data["ext_battery"] if data["ext_battery"] != 655.35 else -1
                    gps_log.raw = data["message_raw"]
                    gps_log.save()
                    gps_dev.charging = True if float(gps_log.ext_battery) > 3. else False
                    gps_dev.save()
                    print("Log saved")
            except Exception as log_err:
                print("Exception saving log: {}".format(log_err))
                return HttpResponse(content=log_err, status=418)  # I'm a teapot
        except Exception as add_err:
            print("Exception detected: {}".format(add_err))
            return HttpResponse(content=add_err, status=418)  # I'm a teapot
    else:
        return HttpResponse(status=405)  # Method not allowed
    return HttpResponse(status=201)  # OK


@csrf_exempt
def add_multiple_log(request):
    print("Received a new set of logs:")
    if request.method == "POST":
        try:
            fails = []
            data = json.loads(request.body.decode("utf-8", "strict"))
            for idx, log in enumerate(data):
                try:
                    gps_imei = log["id"]
                    gps_model = "M"
                    gps_dev = GPSDevices.objects.get(imei=gps_imei, model=gps_model)
                except ObjectDoesNotExist:
                    gps_dev = GPSDevices()
                    gps_dev.imei = log["id"]
                    gps_dev.model = "M"
                    if log["server"] == "pt":
                        gps_dev.server = "PROD PT"
                    elif log["server"] == "es":
                        gps_dev.server = "PROD ES"
                    elif log["server"] == "qa":
                        gps_dev.server = "QA"
                    else:
                        gps_dev.server = log["server"]
                    gps_dev.warning = 1
                    gps_dev.comments = "Created automatically"
                    gps_dev.save()
                    print("New GPS created")
                print("\t-{}".format(gps_dev))
                try:
                    try:
                        # Check if log already exists
                        raw_msg = msg_parser(log["message_raw"])
                        ts = pytz.timezone("Europe/Lisbon").localize(
                            datetime.strptime(str(raw_msg["timestamp"]), "%y%m%d%H%M%S"))
                        tr = pytz.timezone("Europe/Lisbon").localize(
                            datetime.strptime(str(log["timestamp"]), "%y%m%d%H%M%S"))
                        gps_log = GPSLogs.objects.get(gps=gps_dev, timestamp_msg=ts, timestamp_received=tr)
                        print("Log already exists")
                        return HttpResponse(status=409)
                    except MultipleObjectsReturned:
                        print("Log already exists... twice...")
                        return HttpResponse(status=409)
                    except ObjectDoesNotExist:
                        print("Saving log...")
                        gps_log = GPSLogs()
                        gps_log.gps = gps_dev
                        ts = pytz.timezone("Europe/Lisbon").localize(
                            datetime.strptime(str(raw_msg["timestamp"]), "%y%m%d%H%M%S"))
                        gps_log.timestamp_msg = ts
                        if "timestamp_received" in log.keys():
                            ts = pytz.timezone("Europe/Lisbon").localize(
                                datetime.strptime(str(log["timestamp_received"]), "%y%m%d%H%M%S"))
                        else:
                            ts = pytz.timezone("Europe/Lisbon").localize(datetime.now())
                        gps_log.timestamp_received = ts
                        gps_log.latitude = log["latitude"]
                        gps_log.longitude = log["longitude"]
                        gps_log.event = raw_msg["event"]
                        gps_log.speed = log["velocity"]
                        gps_log.valid = log["valid"]
                        gps_log.battery = log["battery"]
                        gps_log.ext_battery = log["ext_battery"] if log["ext_battery"] != 655.35 else -1
                        gps_log.raw = log["message_raw"]
                        gps_log.save()
                        gps_dev.charging = True if float(gps_log.ext_battery) > 3. else False
                        gps_dev.save()
                        print("Log saved")
                except Exception as log_err:
                    print("Exception saving log: {}".format(log_err))
                    fails.append([idx, log])
        except Exception as add_err:
            print("Exception detected: {}".format(add_err))
            return HttpResponse(content=add_err, status=418)  # I'm a teapot
    else:
        return HttpResponse(status=405)  # Method not allowed

    if len(fails) == 0:
        return HttpResponse(status=201)  # Created
    else:
        return HttpResponse(fails, status=207)  # Multistatus


def msg_parser(raw_msg):
    """
    $$<Identifier 1 byte><Length>,<IMEI>,<Command>,<Event>,<Lat>,<Lon>,<yymmddHHMMSS>,<Valid/Invalid>,<Satelites>,
    <GSM 0-31>, <Speed>, <Direction 0=North>,<HDOP accuracy, lower is better>,<Altitude>,<Mileage>,<RunTime>,
    <Base station info>,<I/O port status>,<Analog input value>,<Assisted event info>,<Customized data>,
    <Protocol version>*<Checksum>\r\n
    :param raw_msg:
    :return:
    """
    print('Parsing raw')
    msg_fields = re.split("[,*]", raw_msg)
    msg = {}
    msg["header"] = msg_fields[0]  # Contains '$$', 1 byte identifier and msg length
    msg["imei"] = msg_fields[1]
    msg["command"] = msg_fields[2]
    msg["event"] = msg_fields[3]
    msg["latitude"] = msg_fields[4]
    msg["longitude"] = msg_fields[5]
    msg["timestamp"] = msg_fields[6]
    msg["status"] = msg_fields[7]
    msg["satellites"] = msg_fields[8]
    msg["signal_strenght"] = msg_fields[9]
    msg["speed"] = msg_fields[10]
    msg["direction"] = msg_fields[11]
    msg["hdop"] = msg_fields[12]
    msg["altitude"] = msg_fields[13]
    msg["mileage"] = msg_fields[14]
    msg["runtime"] = msg_fields[15]
    msg["base_station"] = msg_fields[16]
    msg["io_ports"] = msg_fields[17]
    msg["analog_input"] = msg_fields[18]
    # Remaining fields are not sent
    # msg["assisted_event"] = msg_fields[19]
    # msg["customized"] = msg_fields[20]
    # msg["protocol"] = msg_fields[21]
    # msg["checksum"] = msg_fields[22]
    return msg


def update_warnings(request):
    """
    This function will sweep all GPS devices and activate warnings for specific situations
    """
    t_limit = lx_tz.localize(datetime.now())-timedelta(minutes=15)
    gps_devices = GPSDevices.objects.filter(last_check__lt=t_limit)

    for gps in gps_devices:
        # Get current warning and comments
        comments = gps.comments

        # Check all warning signs
        no_logs = False
        bat_low = False
        bat_very_low = False
        ext_bat_low = False
        over_2days = False
        over_12h = False
        wrong_timestamp = False
        past_com = False
        no_charge = False

        # 1. Check if there are logs
        if GPSLogs.objects.filter(gps=gps).count() == 0:
            print("{} - No Logs of this device".format(gps))
            no_logs = True
            if "No logs for device" not in gps.comments:
                gps.comments = gps.comments + "No logs for device; "
        else:
            gps.comments = gps.comments.replace("No logs for device; ", '')
            print("{} - Getting logs".format(gps))

            # Get latest received msg
            last_msg = GPSLogs.objects.filter(gps=gps).latest('timestamp_received')
            print("Last msg: {}".format(last_msg))

            # Check interal battery level
            if last_msg.battery < 3.6:
                bat_very_low = True
                if "Very Low battery" not in gps.comments:
                    gps.comments = gps.comments + "Very Low battery; "
            else:
                gps.comments = gps.comments.replace("Very Low battery; ", '')
                if last_msg.battery < 3.9:
                    bat_low = True
                    if "Low battery" not in gps.comments:
                        gps.comments = gps.comments + "Low battery; "
                else:
                    gps.comments = gps.comments.replace("Low battery; ", '')

            # Check external battery level
            if last_msg.ext_battery < 3.8:
                ext_bat_low = True
                if "Low external battery" not in gps.comments:
                    gps.comments = gps.comments + "Low external battery; "
            else:
                gps.comments = gps.comments.replace("Low external battery; ", '')

            # Check if timestamp of the message and the received are close
            if abs((last_msg.timestamp_received - last_msg.timestamp_msg).total_seconds()) > timedelta(hours=6).total_seconds():
                print("Message timestamp does not match to received timestamp")
                wrong_timestamp = True
                if "Wrong timestamp" not in gps.comments:
                    gps.comments = gps.comments + "Wrong timestamp; "
            else:
                gps.comments = gps.comments.replace("Wrong timestamp; ", '')

            # Check how long ago was the last message received
            if ((lx_tz.localize(datetime.now()) - last_msg.timestamp_received).total_seconds()) > timedelta(days=2).total_seconds():
                over_2days = True
                if "Last communication was over 2 days" not in gps.comments:
                    gps.comments = gps.comments + "Last communication was over 2 days; "
                else:
                    gps.comments = gps.comments.replace("Last communication was over 2 days; ", '')
                    if ((lx_tz.localize(datetime.now()) - last_msg.timestamp_received).total_seconds()) > timedelta(hours=12).total_seconds():
                        over_12h = True
                        if "Last communication was over 12h" not in gps.comments:
                            gps.comments = gps.comments + "Last communication was over 12h; "
                    else:
                        gps.comments = gps.comments.replace("Last communication was over 12h; ", '')

            # Check if last messages received Event 27
            event27 = True
            last_events = GPSLogs.objects.filter(gps=gps).order_by('timestamp_received').values_list('event')[0:10]
            for ev in last_events:
                if ev[0] != 27:
                    event27 = False
                    break
            if event27:
                if "Multiple Event 27 messages" not in gps.comments:
                    gps.comments = gps.comments + "Multiple Event 27 messages; "
                else:
                    gps.comments = gps.comments.replace("Multiple Event 27 messages; ", '')

        # Save
        if ext_bat_low or bat_very_low or over_2days or no_charge:
            gps.warning = 2
        elif no_logs or bat_low or over_12h or wrong_timestamp or past_com:
            gps.warning = 1
        else:
            gps.warning = 0

        gps.save()

    return HttpResponse(status=201)
