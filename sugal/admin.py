from django.contrib import admin

# Register your models here.
from .models import GPSDevices, GPSLogs


class GPSDevicesAdmin(admin.ModelAdmin):
    fields = ["model", "imei", "serial_number", "owner", "box_nr", "phone_number", "license_plate", "state", "warning", "comments"]
    list_display = ("imei", "box_nr", "state", "owner", "phone_number")


class GPSLogsAdmin(admin.ModelAdmin):
    fieldsets = [
        ('GPS',                 {'fields': ['gps']}),
        ('Date information',    {'fields': ['timestamp_msg', 'timestamp_received']}),
        ('Location',            {'fields': ['event', 'valid', 'latitude', 'longitude', 'speed']}),
        ('Battery',             {'fields': ['battery', 'ext_battery']}),
        ('Other',               {'fields': ['raw']})
    ]
    list_display = ["gps", "timestamp_msg", "timestamp_received"]


admin.site.register(GPSDevices, GPSDevicesAdmin)
admin.site.register(GPSLogs, GPSLogsAdmin)
