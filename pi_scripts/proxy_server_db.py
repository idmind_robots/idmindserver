#! /usr/bin/env python

from flask import Flask, request, jsonify, Response
from werkzeug.exceptions import ImATeapot
from datetime import datetime
import getpass
import sqlite3
from sqlite3 import Error, OperationalError
import requests
import json

""" This module implements a public http server that serves as a proxy for the internal http server.

    Listens to post requests from several projects.

    Current list includes LOOP and sugal

    Incoming requests sent to the endpoint defined in this module are validated
        and forwarded to the internal server, if deemed worthy


"""

app = Flask(__name__)

DB_PATH = "/home/pi/sugal_sqlite.db"
BULK_NR = 5


def save_to_db(gps_log):
    """ create a database connection to a SQLite database and save the data to tables """
    conn = None
    c = None
    print("Saving gps log to Local SQlite")
    try:
        conn = sqlite3.connect(DB_PATH)
        c = conn.cursor()
        c.execute("INSERT INTO sugal_logs(log) VALUES(?);", (gps_log, ))
        print("\tSuccessful")
    except OperationalError as e:
        try:
            print("\tCreating new table")
            c.execute("CREATE TABLE IF NOT EXISTS sugal_logs (log text NUT NULL);")
            print("\t\tSuccessful")
            c.execute("INSERT INTO sugal_logs(log) VALUES(?);", (gps_log, ))
            print("\tSuccessful")
        except Error as e:
            print("\t\tFailed to create new table: {}".format(e))
    except Error as e:
        print("\tFailed to save to DB: {}".format(e))
    finally:
        if conn:
            conn.commit()
            conn.close()


def log(message):
    time = str(datetime.now())
    with open("/home/{}/sugal_logs/".format(getpass.getuser()) + str(datetime.now().date()), 'a') as log_file:
        log_file.write(time + message + '\n')


def loop_log(message):
    time = str(datetime.now())
    with open("/home/pi/loop_log.txt", 'a') as log_file:
        log_file.write(time + str(message) + '\n')


def log_gps(origin, data):
    now = datetime.now()
    filename = now.strftime("%y_%m_%d")
    timestamp = now.strftime("%H:%M:%S")
    with open("/home/{}/idmind_scripts/proxy_logs/{}/{}.log".format(getpass.getuser(), origin, filename), "a") as fp:
        fp.write("%s\t%s\n" % (timestamp, json.dumps(data)))
    

def send_bulk():
    conn = None
    try:
        # Connect to Database
        conn = sqlite3.connect(DB_PATH)
        c = conn.cursor()
        # Check number of saved logs
        log_count = c.execute("SELECT count(log) FROM sugal_logs;").fetchall()[0][0]
        if log_count > BULK_NR:
            # Save the logs to a list of dictionary and delete from table
            try:
                data = []
                logs = c.execute("SELECT * FROM sugal_logs order by rowid limit {};".format(BULK_NR)).fetchall()
                for l in logs:
                    data.append(json.loads(l[0]))

                c.execute("DELETE * FROM sugal_logs order by rowid limit {};".format(BULK_NR))
            except Error as e:
                print("Failed to send bulk data to server: {}".format(e))
    except Error as e:
        print("Failed to retrieve data from DB")
    finally:
        if conn:
            conn.commit()
            conn.close()
    # res = requests.post("http://idmind.herokuapp.com/sugal/add_log/", json=data, timeout=5.0)
    return


@app.route('/gps/pt/new', methods=['POST'])
def new_coordinates_pt():
    print('new PT gps request')
    # Save to file
    data = {}
    try:
        data = request.get_json()
        log_gps('pt', data)
    except Exception as err:
        print("Failed to save PT log to file: {}".format(err))

    # Save to IDMind Server
    try:
        data["server"] = "PROD PT"
        try:
            requests.post("http://idmind.herokuapp.com/sugal/add_log/", json=data, timeout=5.0)
        except Exception as err:
            print("FAILED TO POST GPS TO HEROKU: {}".format(err))
    except Exception as err:
        print("FAILED TO POST GPS TO HEROKU: {}".format(err))

    # Save to local DB
    try:
        data["server"] = "PROD PT"
        save_to_db(json.dumps(data))
    except Exception as err:
        print("FAILED TO POST GPS TO Local SQLite: {}".format(err))
    
    # Save to local server
    try:
        requests.post('http://127.0.0.1:5000/location', json=request.json, timeout=1.0)
        print('posted to flask-sqlite')
    except Exception as err:
        print("Failed to POST GPS log to SQLITE: {}".format(err))

    return Response(status=200)


@app.route('/gps/es/new', methods=['POST'])
def new_coordinates_es():
    print('new ES gps request')
    data = {}
    try:
        data = request.get_json()
        log_gps('es', data)
        data["server"] = "PROD ES"
    except Exception as err:
        print('Failed to save ES log to file: {}'.format(err))

    # Save to IDMind Server
    try:
        data["server"] = "PROD ES"
        try:
            requests.post("http://idmind.herokuapp.com/sugal/add_log/", json=data, timeout=5.0)
        except Exception as err:
            print("FAILED TO POST GPS TO HEROKU: {}".format(err))
    except Exception as err:
        print("FAILED TO POST GPS TO HEROKU: {}".format(err))

    try:
        requests.post('http://127.0.0.1:5000/location', json=request.json, timeout=1.0)
        print('posted to flask-sqlite')
    except Exception as err:
        print("Failed to POST GPS log to SQLITE: {}".format(err))

    return Response(status=200)


@app.route('/gps/qa/new', methods=['POST'])
def new_coordinates_qa():
    print('new QA gps request')
    data = {}
    try:
        data = request.get_json()
        log_gps('qa', data)
        data["server"] = "QA"
    except Exception as err:
        print('Failed to save QA log to file: {}'.format(err))

    # Save to IDMind Server
    try:
        data["server"] = "QA"
        try:
            requests.post("http://idmind.herokuapp.com/sugal/add_log/", json=data, timeout=5.0)
        except Exception as err:
            print("FAILED TO POST GPS TO HEROKU: {}".format(err))
    except Exception as err:
        print("FAILED TO POST GPS TO HEROKU: {}".format(err))

    try:
        requests.post('http://127.0.0.1:5000/location', json=request.json, timeout=1.0)
        print('posted to flask-sqlite')
    except Exception as err:
        print("Failed to POST GPS log to SQLITE: {}".format(err))

    return Response(status=200)


@app.route('/loop/log', methods=['POST'])
def new_loop_log():
    print('{}'.format(request))
    data = request.get_json()
    try:
        loop_log(data)
    except:
        print("Exception writing to file")

    try:
        requests.post('http://127.0.0.1:8000/loop/add_log', json=data, timeout=5.0)
    except Exception as err:
        print('Failed to POST Loop Log to Django: {}'.format(err))

    try:
        r = requests.post("http://idmind.herokuapp.com/loop/add_log", json=data, timeout=5.0)
        print("Heroku reply: {}-{}".format(r.status_code, r.content))
    except Exception as err:
        print("Failed to POST Loop Log to Heroku: {}".format(err))

    return jsonify({'success':True})


@app.route('/gira/bike', methods=['POST'])
def new_gira_log():
    try:
        data = request.get_json()
        now = datetime.now()
        filename = now.strftime("%y_%m_%d")
        timestamp = now.strftime("%H:%M:%S")
        with open("/home/{}/idmind_scripts/proxy_logs/gira/{}.log".format(getpass.getuser(), filename), "a") as fp:
            fp.write("%s\t%s\n" % (timestamp, json.dumps(data)))
    except Exception as e:
        print('an unexpected error has occured: %s' % e)
    finally:
        return Response(status=200)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=50001, threaded=True, debug=True)
