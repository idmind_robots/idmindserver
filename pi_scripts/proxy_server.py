#! /usr/bin/env python

from flask import Flask, request, jsonify, Response
from werkzeug.exceptions import ImATeapot
import getpass
import json
import requests
from datetime import datetime


""" This module implements a public http server that serves as a proxy for the internal http server.

    Listens to post requests from several projects.

    Current list includes LOOP and sugal

    Incoming requests sent to the endpoint defined in this module are validated
        and forwarded to the internal server, if deemed worthy


"""


MY_COORDS = {
    "id": "",
    "timestamp": "",
    "latitude": "",
    "longitude": "",
    "valid": "",
    "velocity": "",
    "battery": "",
    "ext_battery": "",
    "raw": "",
}


app = Flask(__name__)


def log(message):
    time = str(datetime.now())
    with open("/home/{}/sugal_logs/".format(getpass.getuser()) + str(datetime.now().date()), 'a') as log_file:
        log_file.write(time + message + '\n')


def loop_log(message):
    time = str(datetime.now())
    with open("/home/pi/loop_log.txt", 'a') as log_file:
        log_file.write(time + str(message) + '\n')


@app.route('/gps/new', methods=['POST'])
def new_coordinates():
    print('new gps request')
    log(str(request))
    print('logged')
    msg = request.get_json()
    print('valid json')
    try:
        requests.post('http://127.0.0.1:8000/sugal/add_log/', json=msg, timeout=5.0)
        print('posted to django')
    except Exception as err:
        print("Failed to POST GPS log to Django: {}".format(err))

#    try:
#        requests.post('http://127.0.0.1:50002/gps/new/', json=msg, timeout=1.0)
#        print('posted to flask-redis')
#    except Exception as err:
#        print("Failed to POST GPS log to REDIS: {}".format(err))

    try:
        requests.post('http://127.0.0.1:5000/location', json=msg, timeout=1.0)
        print('posted to flask-sqlite')
    except Exception as err:
        print("Failed to POST GPS log to SQLITE: {}".format(err))

    return Response(status=200)


def log_gps(origin, data):
    now = datetime.now()
    filename = now.strftime("%y_%m_%d")
    timestamp = now.strftime("%H:%M:%S")
    with open("/home/{}/idmind_scripts/proxy_logs/{}/{}.log".format(getpass.getuser(), origin, filename), "a") as fp:
        fp.write("%s\t%s\n" % (timestamp, json.dumps(data)))


@app.route('/gps/pt/new', methods=['POST'])
def new_coordinates_pt():
    print('new PT gps request')
    try:
        data = request.get_json()
        log_gps('pt', data)
        data["server"] = "PROD PT"
        requests.post('http://127.0.0.1:8000/sugal/add_log/', json=data, timeout=5.0)
    except Exception as err:
        print('Failed to POST GPS PT Log to Django: {}'.format(err))
    try:
        requests.post("http://idmind.herokuapp.com/sugal/add_log/", json=data, timeout=5.0)
    except Exception as err:
        print("FAILED TO POST GPS TO HEROKU: {}".format(err))
    try:
        requests.post('http://127.0.0.1:5000/location', json=request.json, timeout=1.0)
        print('posted to flask-sqlite')
    except Exception as err:
        print("Failed to POST GPS log to SQLITE: {}".format(err))

    return Response(status=200)


@app.route('/gps/es/new', methods=['POST'])
def new_coordinates_es():
    print('new ES gps request')
    try:
        data = request.get_json()
        log_gps('es', data)
        data["server"] = "PROD ES"
        requests.post('http://127.0.0.1:8000/sugal/add_log/', json=data, timeout=5.0)
    except Exception as err:
        print('Failed to POST GPS ES Log to Django: {}'.format(err))

    try:
        requests.post("http://idmind.herokuapp.com/sugal/add_log/", json=data, timeout=5.0)
    except Exception as err:
        print("FAILED TO POST GPS TO HEROKU: {}".format(err))

    try:
        requests.post('http://127.0.0.1:5000/location', json=request.json, timeout=1.0)
        print('posted to flask-sqlite')
    except Exception as err:
        print("Failed to POST GPS log to SQLITE: {}".format(err))

    return Response(status=200)


@app.route('/gps/qa/new', methods=['POST'])
def new_coordinates_qa():
    print('new QA gps request')
    try:
        data = request.get_json()
        log_gps('qa', data)
        data["server"] = "QA"
        requests.post('http://127.0.0.1:8000/sugal/add_log/', json=data, timeout=5.0)
    except Exception as err:
        print('Failed to POST GPS QA Log to Django: {}'.format(err))

    try:
        requests.post("http://idmind.herokuapp.com/sugal/add_log/", json=data, timeout=5.0)
    except Exception as err:
        print("FAILED TO POST GPS TO HEROKU: {}".format(err))

    try:
        requests.post('http://127.0.0.1:5000/location', json=request.json, timeout=1.0)
        print('posted to flask-sqlite')
    except Exception as err:
        print("Failed to POST GPS log to SQLITE: {}".format(err))

    return Response(status=200)


@app.route('/loop/log', methods=['POST'])
def new_loop_log():
    print('{}'.format(request))
    data = request.get_json()
    try:
        loop_log(data)
    except:
        print("Exception writing to file")

    try:
        requests.post('http://127.0.0.1:8000/loop/add_log', json=data, timeout=5.0)
    except Exception as err:
        print('Failed to POST Loop Log to Django: {}'.format(err))

    try:
        r = requests.post("http://idmind.herokuapp.com/loop/add_log", json=data, timeout=5.0)
        print("Heroku reply: {}-{}".format(r.status_code, r.content))
    except Exception as err:
        print("Failed to POST Loop Log to Heroku: {}".format(err))

    return jsonify({'success':True})


@app.route('/gira/bike', methods=['POST'])
def new_gira_log():
    try:
        data = request.get_json()
        now = datetime.now()
        filename = now.strftime("%y_%m_%d")
        timestamp = now.strftime("%H:%M:%S")
        with open("/home/{}/idmind_scripts/proxy_logs/gira/{}.log".format(getpass.getuser(), filename), "a") as fp:
            fp.write("%s\t%s\n" % (timestamp, json.dumps(data)))
    except Exception as e:
        print('an unexpected error has occured: %s' % e)
    finally:
        return Response(status=200)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=50001, threaded=True, debug=True)
