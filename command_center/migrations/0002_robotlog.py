# Generated by Django 2.2 on 2019-09-05 15:52

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('command_center', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RobotLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Timestamp')),
                ('node', models.CharField(default='', max_length=50, verbose_name='Node')),
                ('data', models.CharField(default='', max_length=500, verbose_name='Data')),
                ('robot', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='command_center.RobotDetails')),
            ],
            options={
                'verbose_name_plural': 'Robot Logs',
                'ordering': ('robot', 'timestamp'),
            },
        ),
    ]
