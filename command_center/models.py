from django.db import models
from django.utils import timezone


# Create your models here.
class Projects(models.Model):
    name = models.CharField(max_length=50, unique=True)
    project_start = models.DateTimeField('Start', default=timezone.now)
    active = models.BooleanField(default=True, verbose_name="Active Project")
    status = models.CharField(max_length=100,
                              choices=(("D", "Development"), ("De", "Deployment"), ("M", "Maintenance")), default="D")
    app_name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    def get_start(self):
        return self.project_start.strftime("%d %b %Y, %H:%M:%S")

    def get_status(self):
        return self.get_status_display()

    def is_active(self):
        return self.active

    class Meta:
        ordering = ('active', 'project_start',)
        verbose_name_plural = 'Projects'


class RobotModels(models.Model):
    name = models.CharField(max_length=100, verbose_name='Robot Model', default="Fishy", primary_key=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Robot Models'


class RobotDetails(models.Model):
    name = models.CharField(max_length=100, verbose_name='Robot Name', primary_key=True)
    model = models.ForeignKey(RobotModels, on_delete=models.CASCADE)
    project = models.ForeignKey(Projects, on_delete=models.CASCADE, blank=True, default="")
    ip = models.GenericIPAddressField(verbose_name='IP Address', default="127.0.0.1")

    def __str__(self):
        return "{} robot, with IP {}".format(self.name, self.ip)

    class Meta:
        ordering = ('model', 'name',)
        verbose_name_plural = 'Robot Details'


class RobotLog(models.Model):
    robot = models.ForeignKey(RobotDetails, on_delete=models.CASCADE, null=True)
    timestamp = models.DateTimeField(verbose_name="Timestamp", default=timezone.now)
    node = models.CharField(max_length=50, verbose_name="Node", default="")
    data = models.CharField(max_length=500, verbose_name="Data", default="")

    def __str__(self):
        return "Time: {}\nNode: {}\nMessage: {}\n".\
            format(self.timestamp.strftime("%Y-%m-%d %H:%M"), self.node, self.data)

    class Meta:
        ordering = ('robot', 'timestamp',)
        verbose_name_plural = 'Robot Logs'