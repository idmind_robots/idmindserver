from django.contrib import admin
from .models import Projects, RobotDetails, RobotModels, RobotLog


# Register your models here.
class ProjectsAdmin(admin.ModelAdmin):
    list_display = ['name', 'project_start', 'active', 'status']


class RobotModelsAdmin(admin.ModelAdmin):
    list_display = ["name"]


class RobotDetailsAdmin(admin.ModelAdmin):
    list_display = ["name", "ip"]


class RobotLogAdmin(admin.ModelAdmin):
    list_display = ["robot", "timestamp"]


admin.site.register(Projects, ProjectsAdmin)
admin.site.register(RobotModels, RobotModelsAdmin)
admin.site.register(RobotDetails, RobotDetailsAdmin)
admin.site.register(RobotLog, RobotLogAdmin)
