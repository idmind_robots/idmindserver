from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, DetailView, UpdateView, DeleteView
from .models import Projects, RobotModels, RobotDetails
from django.contrib.auth.mixins import LoginRequiredMixin


# Consider using a different kind of view, that does not require context
class MainMenu(LoginRequiredMixin, ListView):
    login_url = '/accounts/login'
    model = Projects
    template_name = 'command_center/command_center.html'
    context_object_name = 'projects'


class ProjectListView(LoginRequiredMixin, ListView):
    login_url = '/accounts/login'
    model = Projects
    template_name = 'command_center/project_list.html'
    context_object_name = 'projects'


class ProjectDetailView(LoginRequiredMixin, DetailView):
    login_url = '/accounts/login'
    model = Projects
    template_name = 'command_center/project_list.html'
    context_object_name = 'projects'


class RobotListView(LoginRequiredMixin, ListView):
    login_url = '/accounts/login'
    model = RobotDetails
    template_name = 'command_center/robot_list.html'
    context_object_name = 'robots'

    def get_context_data(self, **kwargs):
        # Adds information to the context
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['models'] = RobotModels.objects.all()
        return context
