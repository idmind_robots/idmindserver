from django.urls import path

from . import views

app_name = 'command_center'
urlpatterns = [
    path('', views.MainMenu.as_view(), name='main_menu'),
    path('command/', views.MainMenu.as_view(), name='main_menu'),
    path('projects/', views.ProjectListView.as_view(), name='project_list'),
    path('projects/<pk>', views.ProjectDetailView.as_view(), name='project_detail'),    # This is called for projects that do not have their own app
    path('robot_list/', views.RobotListView.as_view(), name='robot_list'),
    # path('robot_list/<model>', views.RobotListView.as_view(), name='robot_list_view'),
    # # path('robot_details/<pk>', views.RobotDetailsView.as_view(), name='robot_details_view'),
    # path('robot_details/<pk>', views.RobotDetailsView.as_view(), name='robot_details_view'),
    # path('fish_tanks/', views.FishTanksView.as_view(), name='fish_tanks_view'),
    # path('ros_interface/', views.ROSInterface.as_view(), name='ros_interface'),
    # path('ros_interface/<model>', views.ROSInterface.as_view(), name='ros_interface'),
]
