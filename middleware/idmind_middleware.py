from django.urls import reverse
from command_center.models import Projects, RobotModels


class SimpleMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        if not request.path.startswith(reverse('admin:index')):
            g = request.GET.copy()
            g["navbar_projects"] = Projects.objects.all().order_by('active')
            g["navbar_models"] = RobotModels.objects.all().order_by('name')
            request.GET = g

        response = self.get_response(request)
        # Code to be executed for each request/response after
        # the view is called.

        return response
