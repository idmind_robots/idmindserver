# idmindserver

IDMind&#39;s Webserver revised in August 2019

## Templates
The `templates` folder contains the `base.html` template and an empty app template.
Each app should make use of the base template and create a specific template under `<app>/templates/<app>`

## Static Files
By default, each app searches for static files in `<app>/static`.
In the settings, we have included the `static` folder as source of static files as well.
For the server, all static folders should include `css`, `js` and `img` folders.

## Connecting to MySQL database
For development:
* https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04
* MySQL >= 5.6
* mysqlclient - https://pypi.org/project/mysqlclient/
* Load timezone tables in MySQL server - https://dev.mysql.com/doc/refman/8.0/en/mysql-tzinfo-to-sql.html (?) 

For production:
* https://www.codementor.io/jamesezechukwu/how-to-deploy-django-app-on-heroku-dtsee04d4
* heroku config:set SECRETKEY=<SECRET_KEY>
* heroku config:set DATABASEURL = mysql://user:password@host:port/database

